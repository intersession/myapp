<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration {

	public function up()
	{
		Schema::create('pages', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 100);
			$table->text('content')->nullable();
		});
	}

	public function down()
	{
		if(Schema::hasTable('pages')){
			Schema::drop('pages');
		}
	}
}
