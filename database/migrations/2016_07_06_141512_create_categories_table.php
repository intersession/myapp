<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	public function up()
	{
		Schema::connection('mysql_api')->create('categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('description', 500);
		});
	}

	public function down()
	{
		if(Schema::connection('mysql_api')->hasTable('categories')){
			Schema::connection('mysql_api')->drop('categories');
		}
	}
}
