<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEntitiesTable extends Migration {

	public function up()
	{
		Schema::connection('mysql_api')->create('entities', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->integer('id_categories')->unsigned();
		});
	}

	public function down()
	{
		if(Schema::connection('mysql_api')->hasTable('entities')){
			Schema::connection('mysql_api')->drop('entities');
		}
	}
}
