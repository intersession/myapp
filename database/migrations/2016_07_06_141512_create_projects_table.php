<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	public function up()
	{
		Schema::create('projects', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_users')->unsigned();
			$table->string('slug', 50);
			$table->string('bdd_password', 100);
			$table->string('name', 50);
			$table->string('description', 500);
		});
	}

	public function down()
	{
		if(Schema::hasTable('projects')){
			Schema::drop('projects');
		}
	}
}
