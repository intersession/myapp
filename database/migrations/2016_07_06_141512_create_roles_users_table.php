<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesUsersTable extends Migration {

	public function up()
	{
		Schema::create('roles_users', function(Blueprint $table) {
			$table->integer('id_roles')->unsigned();
			$table->integer('id_users')->unsigned();
		});
	}

	public function down()
	{
		if(Schema::hasTable('roles_users')){
			Schema::drop('roles_users');
		}
	}
}
