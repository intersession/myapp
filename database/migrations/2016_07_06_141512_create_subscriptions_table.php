<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionsTable extends Migration {

	public function up()
	{
		Schema::create('subscriptions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_users')->unsigned()->index();
			$table->integer('level');
			$table->date('starting_date');
			$table->date('ending_date');
		});
	}

	public function down()
	{
		if(Schema::hasTable('subscriptions')){
			Schema::drop('subscriptions');
		}
	}
}
