<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColumnsTable extends Migration {

	public function up()
	{
		Schema::connection('mysql_api')->create('columns', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_categories')->unsigned()->nullable();
			$table->integer('id_tables')->unsigned();
			$table->integer('length');
			$table->string('type', 50);
			$table->string('name', 100);
			$table->boolean('required');
		});
	}

	public function down()
	{
		if(Schema::connection('mysql_api')->hasTable('columns')){
			Schema::connection('mysql_api')->drop('columns');
		}
	}
}
