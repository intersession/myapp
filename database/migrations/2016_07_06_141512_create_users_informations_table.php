<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersInformationsTable extends Migration {

	public function up()
	{
		Schema::create('users_informations', function(Blueprint $table) {
			$table->integer('id_users')->unsigned()->index();
			$table->string('address', 100);
			$table->string('city', 50);
			$table->string('postal_code', 15);
			$table->date('birthday');
			$table->increments('id');
		});
	}

	public function down()
	{
		if(Schema::hasTable('users_informations')){
			Schema::drop('users_informations');
		}
	}
}
