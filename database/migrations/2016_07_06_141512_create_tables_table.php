<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTablesTable extends Migration {

	public function up()
	{
		Schema::connection('mysql_api')->create('tables', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_projects')->unsigned();
			$table->string('name', 100);
			$table->string('description', 200);
		});
	}

	public function down()
	{
		if(Schema::connection('mysql_api')->hasTable('tables')){
			Schema::connection('mysql_api')->drop('tables');
		}

		if(Schema::connection('mysql_customer')->hasTable('tableone')){
			Schema::connection('mysql_customer')->drop('tableone');
		}
		if(Schema::connection('mysql_customer')->hasTable('tabletwo')){
			Schema::connection('mysql_customer')->drop('tabletwo');
		}
		if(Schema::connection('mysql_customer')->drop('tablethree')){
			Schema::connection('mysql_customer')->drop('tablethree');
		}
	}
}
