<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('email', 100);
			$table->string('lastname', 25);
			$table->string('password', 100);
			$table->string('firstname', 25);
			$table->string('permissions', 200);
			$table->integer('id_companies')->unsigned()->index();
			$table->rememberToken('remember_token');
			$table->datetime('updated_at')->nullable();
		});
	}

	public function down()
	{
		if(Schema::hasTable('users')){
			Schema::drop('users');
		}
	}
}
