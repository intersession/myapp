<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBanksTable extends Migration {

	public function up()
	{
		Schema::create('banks', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_subscriptions')->unsigned();
			$table->string('iban', 34);
			$table->string('bic', 11);
			$table->string('name', 50);
			$table->string('address', 100);
			$table->string('city', 50);
			$table->string('postal_code', 15);
		});
	}

	public function down()
	{
		if(Schema::hasTable('banks')){
			Schema::drop('banks');
		}
	}
}
