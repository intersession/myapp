<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->foreign('id_companies')->references('id')->on('companies')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('subscriptions', function(Blueprint $table) {
			$table->foreign('id_users')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('users_informations', function(Blueprint $table) {
			$table->foreign('id_users')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('banks', function(Blueprint $table) {
			$table->foreign('id_subscriptions')->references('id')->on('subscriptions')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('projects', function(Blueprint $table) {
			$table->foreign('id_users')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('roles_users', function(Blueprint $table) {
			$table->foreign('id_roles')->references('id')->on('roles')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('roles_users', function(Blueprint $table) {
			$table->foreign('id_users')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::connection('mysql_api')->table('entities', function(Blueprint $table) {
			$table->foreign('id_categories')->references('id')->on('categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::connection('mysql_api')->table('columns', function(Blueprint $table) {
			$table->foreign('id_categories')->references('id')->on('categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::connection('mysql_api')->table('columns', function(Blueprint $table) {
			$table->foreign('id_tables')->references('id')->on('tables')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('sessions', function(Blueprint $table) {
			$table->foreign('id_users')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->dropForeign('users_id_companies_foreign');
		});
		Schema::table('subscriptions', function(Blueprint $table) {
			$table->dropForeign('subscriptions_id_users_foreign');
		});
		Schema::table('users_informations', function(Blueprint $table) {
			$table->dropForeign('users_informations_id_users_foreign');
		});
		Schema::table('banks', function(Blueprint $table) {
			$table->dropForeign('banks_id_subscriptions_foreign');
		});
		Schema::table('projects', function(Blueprint $table) {
			$table->dropForeign('projects_id_users_foreign');
		});
		Schema::table('roles_users', function(Blueprint $table) {
			$table->dropForeign('roles_users_id_roles_foreign');
		});
		Schema::table('roles_users', function(Blueprint $table) {
			$table->dropForeign('roles_users_id_users_foreign');
		});
		Schema::connection('mysql_api')->table('entities', function(Blueprint $table) {
			$table->dropForeign('entities_id_categories_foreign');
		});
		Schema::connection('mysql_api')->table('columns', function(Blueprint $table) {
			$table->dropForeign('columns_id_categories_foreign');
		});
		Schema::connection('mysql_api')->table('columns', function(Blueprint $table) {
			$table->dropForeign('columns_id_tables_foreign');
		});
		Schema::table('sessions', function(Blueprint $table) {
			$table->dropForeign('sessions_id_users_foreign');
		});
	}
}
