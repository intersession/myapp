<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
			$table->increments('id');
			$table->string('address', 100);
			$table->string('city', 50);
			$table->string('postal_code', 15);
			$table->string('name', 50);
		});
	}

	public function down()
	{
		if(Schema::hasTable('companies')){
			Schema::drop('companies');
		}
	}
}
