<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSessionsTable extends Migration {

	public function up()
	{
		Schema::create('sessions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_users')->unsigned();
			$table->datetime('starting_date');
			$table->datetime('ending_date');
			$table->string('token', 100);
			$table->date('date');
		});
	}

	public function down()
	{
		if(Schema::hasTable('sessions')){
			Schema::drop('sessions');
		}
	}
}
