<?php

use Illuminate\Database\Seeder;
use App\Companies;

class UsersTableSeeder extends Seeder
{

  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $faker = Faker\Factory::create();
    $company = Companies::all()->lists('id')->all();

    DB::table('users')->insert([
      'firstname' => 'John',
      'lastname' => 'Doe',
      'email' =>'j.doe@gmail.com',
      'password' => bcrypt('secret'),
      'id_companies' => $faker->randomElement($company),
      'id' => 1,
    ]);
    DB::table('users')->insert([
      'firstname' => 'Marcel',
      'lastname' => 'Dave',
      'email' =>'m.dave@gmail.com',
      'password' => bcrypt('secret'),
      'id_companies' => $faker->randomElement($company),
      'id' => 2,
    ]);
  }

}
