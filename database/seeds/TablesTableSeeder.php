<?php

use Illuminate\Database\Seeder;
use App\Projects;
use App\Tables;

class TablesTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::connection('mysql_api')->table('tables')->insert([
      'name' => 'TableOne',
      'description' => 'Ceci est une description de la table un.',
      'id_projects' => 1,
      'id' => 1
    ]);
    $this->createTableDB($this->slugify('TableOne'));

    DB::connection('mysql_api')->table('tables')->insert([
      'name' => 'TableTwo',
      'description' => 'Ceci est une description de la table deux.',
      'id_projects' => 1,
      'id' => 2
    ]);
    $this->createTableDB($this->slugify('TableTwo'));

    DB::connection('mysql_api')->table('tables')->insert([
      'name' => 'TableThree',
      'description' => 'Ceci est une description de la table trois.',
      'id_projects' => 1,
      'id' => 3
    ]);
    $this->createTableDB($this->slugify('TableThree'));
  }

  /**
  * Create the table in myapp_customer
  * @param String $tableName
  */
  private function createTableDB($tableName)
  {
      Schema::connection('mysql_customer')->create($tableName, function($table) {
          $table->bigIncrements('id');
          $table->timestamps();
      });
  }

  /**
  * @param $string
  * @return mixed|string
  * Generate slug from given string
  *
  */
  private function slugify($string)
  {
      $string = preg_replace('~[^\pL\d]+~u', '_', $string);
      $string = iconv('utf-8', 'us-ascii//TRANSLIT', $string);
      $string = preg_replace('~[^-\w]+~', '', $string);
      $string = trim($string, '_');
      $string = preg_replace('~-+~', '_', $string);
      $string = strtolower($string);

      return $string;
  }

}
