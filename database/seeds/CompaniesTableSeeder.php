<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('companies')->insert([
      'address' => '15 rue des Roses',
      'city' => 'Lyon',
      'postal_code' => '69006',
      'name' => 'CompanyName',
    ]);
    DB::table('companies')->insert([
      'address' => '86 avenue du Char',
      'city' => 'Lyon',
      'postal_code' => '69001',
      'name' => 'NameCompany',
    ]);
  }
}
