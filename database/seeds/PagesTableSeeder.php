<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('pages')->insert([
      'name' => 'Présentation générale',
      'content' => '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?<br />
        But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</p>
        '
    ]);
    DB::table('pages')->insert([
      'name' => 'Services 1',
      'content' => '<ul>
	                    <li>Service 1</li>
                        <li>Service 2</li>
	                    <li>Service 3</li>
                    </ul>'
    ]);
    DB::table('pages')->insert([
      'name' => 'Services 2',
      'content' => '<ul>
                      <li>Service 2</li>
                      <li>Service 3</li>
                      <li>Service 4</li>
                    </ul>'
    ]);
    DB::table('pages')->insert([
      'name' => 'Services 3',
      'content' => '<ul>
	                    <li>Service 3</li>
                        <li>Service 4</li>
	                    <li>Service 5</li>
                    </ul>'
    ]);
    DB::table('pages')->insert([
        'name' => 'Accroche',
        'content' => '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>'
    ]);
    DB::table('pages')->insert([
        'name' => 'Mentions légales',
        'content' => '<h1>Mentions légales</h1>
</br>
Merci de lire avec attention les différentes modalités d’utilisation du présent site avant d’y parcourir ses pages. En vous connectant sur ce site, vous acceptez sans réserves les présentes modalités. Aussi, conformément à l’article n°6 de la Loi n°2004-575 du 21 Juin 2004 pour la confiance dans l’économie numérique, les responsables du présent site internet <a href="http://www.business-management-system.com">www.business-management-system.com</a> sont :

<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Editeur du Site : </b></span></p>
BMS
Numéro de SIRET : 42079370500047
Responsable editorial : Mouhamed BEN HAMRA
6 Place Charles Hernu, 69100 Villeurbanne
Téléphone :04 82 53 81 64 - Fax : 04 82 53 81 64
Email : contact@business-management-system.com
Site Web : <a href="http://www.business-management-system.com">www.business-management-system.com</a>
</br>
<p style="color: #b51a00;"><b><span style="color: rgb(0, 0, 0);">Hébergement :</span> </b></p>
Hébergeur : OVH
Rue de l\'hebergement
Site Web : <a href="http://69000 Villeurbanne">69000 Villeurbanne</a>
</br>
<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Développement</b><b> : </b></span></p>
Ip Team
Adresse : 69000
Site Web : <a href="http://http://www.ecole-ipssi.com/">http://www.ecole-ipssi.com/</a>
</br>
<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Conditions d’utilisation : </b></span></p>
Ce site (<a href="http://www.business-management-system.com">www.business-management-system.com</a>) est proposé en différents langages web (HTML, HTML5, Javascript, CSS, etc…) pour un meilleur confort d\'utilisation et un graphisme plus agréable, nous vous recommandons de recourir à des navigateurs modernes comme Internet explorer, Safari, Firefox, Google Chrome, etc…
Les mentions légales ont été générées sur le site <a title="générateur de mentions légales pour site internet gratuit" href="http://www.generateur-de-mentions-legales.com">Générateur de mentions légales</a>, offert par <a title="imprimerie paris, imprimeur paris" href="http://welye.com">Welye</a>.

<span style="color: #323333;">BMS<b> </b></span>met en œuvre tous les moyens dont elle dispose, pour assurer une information fiable et une mise à jour fiable de ses sites internet. Toutefois, des erreurs ou omissions peuvent survenir. L\'internaute devra donc s\'assurer de l\'exactitude des informations auprès de , et signaler toutes modifications du site qu\'il jugerait utile. n\'est en aucun cas responsable de l\'utilisation faite de ces informations, et de tout préjudice direct ou indirect pouvant en découler.

<b>Cookies</b> : Le site <a href="http://www.business-management-system.com">www.business-management-system.com</a> peut-être amené à vous demander l’acceptation des cookies pour des besoins de statistiques et d\'affichage. Un cookies est une information déposée sur votre disque dur par le serveur du site que vous visitez. Il contient plusieurs données qui sont stockées sur votre ordinateur dans un simple fichier texte auquel un serveur accède pour lire et enregistrer des informations . Certaines parties de ce site ne peuvent être fonctionnelles sans l’acceptation de cookies.

<b>Liens hypertextes :</b> Les sites internet de peuvent offrir des liens vers d’autres sites internet ou d’autres ressources disponibles sur Internet. BMS ne dispose d\'aucun moyen pour contrôler les sites en connexion avec ses sites internet. ne répond pas de la disponibilité de tels sites et sources externes, ni ne la garantit. Elle ne peut être tenue pour responsable de tout dommage, de quelque nature que ce soit, résultant du contenu de ces sites ou sources externes, et notamment des informations, produits ou services qu’ils proposent, ou de tout usage qui peut être fait de ces éléments. Les risques liés à cette utilisation incombent pleinement à l\'internaute, qui doit se conformer à leurs conditions d\'utilisation.

Les utilisateurs, les abonnés et les visiteurs des sites internet de ne peuvent mettre en place un hyperlien en direction de ce site sans l\'autorisation expresse et préalable de BMS.

Dans l\'hypothèse où un utilisateur ou visiteur souhaiterait mettre en place un hyperlien en direction d’un des sites internet de BMS, il lui appartiendra d\'adresser un email accessible sur le site afin de formuler sa demande de mise en place d\'un hyperlien. BMS se réserve le droit d’accepter ou de refuser un hyperlien sans avoir à en justifier sa décision.
</br>
<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Services fournis : </b></span></p>
<p style="color: #323333;">L\'ensemble des activités de la société ainsi que ses informations sont présentés sur notre site <a href="http://www.business-management-system.com">www.business-management-system.com</a>.</p>
<p style="color: #323333;">BMS s’efforce de fournir sur le site www.business-management-system.com des informations aussi précises que possible. les renseignements figurant sur le site <a href="http://www.business-management-system.com">www.business-management-system.com</a> ne sont pas exhaustifs et les photos non contractuelles. Ils sont donnés sous réserve de modifications ayant été apportées depuis leur mise en ligne. Par ailleurs, tous les informations indiquées sur le site www.business-management-system.com<span style="color: #000000;"><b> </b></span>sont données à titre indicatif, et sont susceptibles de changer ou d’évoluer sans préavis. </p>
</br>
<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Limitation contractuelles sur les données : </b></span></p>
Les informations contenues sur ce site sont aussi précises que possible et le site remis à jour à différentes périodes de l’année, mais peut toutefois contenir des inexactitudes ou des omissions. Si vous constatez une lacune, erreur ou ce qui parait être un dysfonctionnement, merci de bien vouloir le signaler par email, à l’adresse contact@business-management-system.com, en décrivant le problème de la manière la plus précise possible (page posant problème, type d’ordinateur et de navigateur utilisé, …).

Tout contenu téléchargé se fait aux risques et périls de l\'utilisateur et sous sa seule responsabilité. En conséquence, ne saurait être tenu responsable d\'un quelconque dommage subi par l\'ordinateur de l\'utilisateur ou d\'une quelconque perte de données consécutives au téléchargement. <span style="color: #323333;">De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour</span>

Les liens hypertextes mis en place dans le cadre du présent site internet en direction d\'autres ressources présentes sur le réseau Internet ne sauraient engager la responsabilité de BMS.
</br>
<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Propriété intellectuelle :</b></span></p>
Tout le contenu du présent sur le site <a href="http://www.business-management-system.com">www.business-management-system.com</a>, incluant, de façon non limitative, les graphismes, images, textes, vidéos, animations, sons, logos, gifs et icônes ainsi que leur mise en forme sont la propriété exclusive de la société à l\'exception des marques, logos ou contenus appartenant à d\'autres sociétés partenaires ou auteurs.

Toute reproduction, distribution, modification, adaptation, retransmission ou publication, même partielle, de ces différents éléments est strictement interdite sans l\'accord exprès par écrit de BMS. Cette représentation ou reproduction, par quelque procédé que ce soit, constitue une contrefaçon sanctionnée par les articles L.335-2 et suivants du Code de la propriété intellectuelle. Le non-respect de cette interdiction constitue une contrefaçon pouvant engager la responsabilité civile et pénale du contrefacteur. En outre, les propriétaires des Contenus copiés pourraient intenter une action en justice à votre encontre.
</br>
<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Déclaration à la CNIL : </b></span></p>
Conformément à la loi 78-17 du 6 janvier 1978 (modifiée par la loi 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l\'égard des traitements de données à caractère personnel) relative à l\'informatique, aux fichiers et aux libertés, ce site n\'a pas fait l\'objet d\'une déclaration auprès de la Commission nationale de l\'informatique et des libertés (<a href="http://www.cnil.fr/">www.cnil.fr</a>).
</br>
<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Litiges : </b></span></p>
Les présentes conditions du site <a href="http://www.business-management-system.com">www.business-management-system.com</a> sont régies par les lois françaises et toute contestation ou litiges qui pourraient naître de l\'interprétation ou de l\'exécution de celles-ci seront de la compétence exclusive des tribunaux dont dépend le siège social de la société. La langue de référence, pour le règlement de contentieux éventuels, est le français.
</br>
<p style="color: #b51a00;"><span style="color: rgb(0, 0, 0);"><b>Données personnelles :</b></span></p>
De manière générale, vous n’êtes pas tenu de nous communiquer vos données personnelles lorsque vous visitez notre site Internet <a href="http://www.business-management-system.com">www.business-management-system.com</a>.

Cependant, ce principe comporte certaines exceptions. En effet, pour certains services proposés par notre site, vous pouvez être amenés à nous communiquer certaines données telles que : votre nom, votre fonction, le nom de votre société, votre adresse électronique, et votre numéro de téléphone. Tel est le cas lorsque vous remplissez le formulaire qui vous est proposé en ligne, dans la rubrique « contact ». Dans tous les cas, vous pouvez refuser de fournir vos données personnelles. Dans ce cas, vous ne pourrez pas utiliser les services du site, notamment celui de solliciter des renseignements sur notre société, ou de recevoir les lettres d’information.

Enfin, nous pouvons collecter de manière automatique certaines informations vous concernant lors d’une simple navigation sur notre site Internet, notamment : des informations concernant l’utilisation de notre site, comme les zones que vous visitez et les services auxquels vous accédez, votre adresse IP, le type de votre navigateur, vos temps d\'accès. De telles informations sont utilisées exclusivement à des fins de statistiques internes, de manière à améliorer la qualité des services qui vous sont proposés. Les bases de données sont protégées par les dispositions de la loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases de données.'
    ]);
    DB::table('pages')->insert([
        'name' => 'CGV',
        'content' => '<h1>Conditions G&eacute;n&eacute;rales de Vente</h1>

<p><br />
Les pr&eacute;sentes conditions de vente sont conclues d&#39;une part par <a href="http://www.business-management-system.com/">BMS</a> et d&#39;autre part, par toute personne physique ou morale souhaitant proc&eacute;der &agrave; un achat via le site Internet <a href="http://www.business-management-system.com/">BMS</a> d&eacute;nomm&eacute;e ci-apr&egrave;s &laquo; le Client&raquo;.</p>

<p><strong>1- Objet du contrat</strong><br />
Les pr&eacute;sentes conditions g&eacute;n&eacute;rales de vente ont pour objet de fixer les dispositions contractuelles entre <a href="http://www.business-management-system.com/">BMS</a> et le Client, et les conditions applicables &agrave; tout achat effectu&eacute; sur le site. L&rsquo;acquisition d&#39;un bien ou d&#39;un service &agrave; travers le pr&eacute;sent site implique une acceptation sans r&eacute;serve par le Client des pr&eacute;sentes conditions de vente.</p>

<p><strong>2- Caract&eacute;ristiques des produits, biens et services propos&eacute;s</strong><br />
Toutes les caract&eacute;ristiques des produits et services propos&eacute;s par BMS sont pr&eacute;sent&eacute;s sur notre boutique virtuelle sur le site <a href="http://www.business-management-system.com/">BMS</a><br />
Chaque produit est accompagn&eacute; d&rsquo;un descriptif, ainsi que de son prix.</p>

<p><strong>3- Tarifs</strong><br />
BMS se r&eacute;serve le droit de modifier ses prix &agrave; tout moment, &eacute;tant toutefois entendu que le prix figurant au catalogue le jour de la commande sera le seul applicable &agrave; l&rsquo;acheteur.&nbsp;<br />
Les services propos&eacute;s par BMS ne sont en aucun cas affili&eacute; avec Mojang.</p>

<p><strong>4- Commande</strong><br />
Le Client passe commande sur le site Internet de BMS. Pour acheter un ou plusieurs articles, il doit obligatoirement suivre le processus de commande suivant :<br />
&bull; &nbsp; Inscription sur le site<br />
&bull; &nbsp; Cr&eacute;diter son compte en argent virtuel<br />
&bull; &nbsp; Choisir l&rsquo;article<br />
&bull; &nbsp; Valider l&#39;achat et recevoir l&rsquo;achat</p>

<p>BMS se r&eacute;serve le droit d&rsquo;annuler ou de refuser toute commande qui &eacute;manerait d&rsquo;un Client avec lequel il existerait un litige relatif au paiement d&rsquo;une commande pr&eacute;c&eacute;dente. Toute commande vaut acceptation des prix et descriptions des articles disponibles &agrave; la vente.</p>

<p><strong>5- Modalit&eacute;s de paiement</strong><br />
Le prix est exigible &agrave; la commande.&nbsp;<br />
Plusieurs modes de paiements sont possibles.</p>

<p>BMS n&rsquo;est pas responsable d&egrave;s lors que la non validation de la commande r&eacute;sulte de cas de force majeur ou d&rsquo;un probl&egrave;me chez les prestataires.<br />
&nbsp;La s&eacute;curit&eacute; de la transaction financi&egrave;re est la responsabilit&eacute; de nos prestataires Paypal et Starpass.&nbsp;<br />
BMS d&eacute;cline toute responsabilit&eacute; en cas de dysfonctionnement entra&icirc;nant la non validation de la commande r&eacute;sultant de cas de force majeur ou d&rsquo;un probl&egrave;me chez les prestataires.<br />
Pour tout achat avec un prestataire fonctionnant par un syst&egrave;me de micropaiement, le prix est plus &eacute;lev&eacute; qu&#39;avec PayPal ou PaySafeCard pour un m&ecirc;me produit et ce en raison du taux de commission pr&eacute;lev&eacute; par cette soci&eacute;t&eacute;.<br />
L&rsquo;ensemble des donn&eacute;es fournies et la confirmation enregistr&eacute;e vaudront preuve de la transaction.<br />
La confirmation vaudra signature et acceptation des op&eacute;rations effectu&eacute;es.</p>

<p>Tout achat entra&icirc;ne l&#39;acceptation des conditions g&eacute;n&eacute;rales de vente de ces soci&eacute;t&eacute;s :</p>

<p>Paypal<br />
PayPal (Europe) S.&agrave; r.l. &amp; Cie, S.C.A. (&laquo; PayPal Europe &raquo;)<br />
5e &eacute;tage<br />
22-24 Boulevard Royal<br />
L-2449, Luxembourg<br />
(+352) 27 302 143<br />
<a href="https://www.paypal.com">Paypal</a></p>

<p><strong>6- Capacit&eacute; juridique, &acirc;ge</strong><br />
Pour commander sur le site <a href="http://www.business-management-system.com/">BMS</a>, vous attestez de mani&egrave;re ferme et sous votre propre et unique responsabilit&eacute; que :<br />
&bull; &nbsp; Vous avez pleine capacit&eacute; de jouissance et d&rsquo;exercice pour contracter avec nous.<br />
&bull; &nbsp; Vous d&eacute;clarez &ecirc;tre &acirc;g&eacute; d&rsquo;au moins 18 ans et avoir la capacit&eacute; juridique de conclure le pr&eacute;sent contrat. Il ne peut pas nous &ecirc;tre exig&eacute; de v&eacute;rifier l&rsquo;&acirc;ge des acheteurs du site.<br />
Si l&rsquo;acheteur est une personne physique mineure il se doit d&#39;obtenir le consentement de ses parents/tuteurs avant de passer commande. L&#39;autorit&eacute; parentale reconna&icirc;t quant &agrave; elle avoir accept&eacute; les conditions g&eacute;n&eacute;rales et se porte garant du Client mineur. Toute utilisation du site <a href="http://www.business-management-system.com/">BMS</a> et de ses services par le Client mineur est r&eacute;alis&eacute;e sous l&#39;enti&egrave;re responsabilit&eacute; des titulaires de l&#39;autorit&eacute; parentale.</p>

<p><br />
<strong>7- Exp&eacute;dition et d&eacute;lais de livraison</strong><br />
D&egrave;s qu&rsquo;une commande est valid&eacute;e (soit apr&egrave;s son paiement effectif) BMS cr&eacute;ditera en argent virtuel, selon le montant command&eacute;, le compte du Client concern&eacute;.<br />
Les d&eacute;lais d&#39;accr&eacute;ditation de l&#39;argent d&eacute;pendent du service de paiement choisi.<br />
Toute commande pass&eacute;e sur le site <a href="http://www.business-management-system.com/">BMS</a> est livr&eacute;e imm&eacute;diatement &agrave; compter du moment o&ugrave; l&#39;argent virtuel a &eacute;t&eacute; d&eacute;bit&eacute; du compte du Client concern&eacute;.&nbsp;<br />
BMS s&rsquo;engage &agrave; livrer les commandes pass&eacute;es par le Client dans les d&eacute;lais pr&eacute;vus. Si les dits articles n&rsquo;ont pas &eacute;t&eacute; livr&eacute;s dans un d&eacute;lai de sept (7) jours &agrave; compter de la date de livraison pr&eacute;vue lors de la commande, et si ce d&eacute;passement n&rsquo;est pas li&eacute; &agrave; un cas de force majeure, le Joueur pourra proc&eacute;der &agrave; la r&eacute;solution de la vente, en contactant BMS.<br />
Les sommes r&eacute;gl&eacute;es par le Client lui seront alors int&eacute;gralement rembours&eacute;es en argent virtuel.</p>

<p><strong>8- Responsabilit&eacute;</strong><br />
BMS, dans le processus de vente en ligne, n&rsquo;est tenu que par une obligation de moyens; sa responsabilit&eacute; ne pourra &ecirc;tre engag&eacute;e pour un dommage r&eacute;sultant de perturbations du r&eacute;seau Internet tel que perte de donn&eacute;es, intrusion, virus, rupture du service, autres probl&egrave;mes involontaires ou d&rsquo;un probl&egrave;me chez les prestataires (voir 5).</p>

<p><strong>9- R&eacute;tractation et remboursement</strong><br />
Par d&eacute;rogation &agrave; l&rsquo;article L.121-20-2 du Code fran&ccedil;ais de la consommation et compte tenu de la nature du service (service immat&eacute;riel et livr&eacute; sans d&eacute;lai) le Client n&rsquo;a plus droit de r&eacute;tractation &agrave; compter de la date et heure de fourniture du service (article).<br />
Toutefois le Client a un droit de r&eacute;tractation de 14 jours si le service command&eacute; ne lui a pas encore &eacute;t&eacute; livr&eacute; alors que son paiement est effectif.<br />
Dans ce cas il contactera BMS en fournissant l&rsquo;ensemble des indications n&eacute;cessaires (compte utilisateur, num&eacute;ro de paiement, etc.). BMS proc&eacute;dera au remboursement en argent virtuel dans un d&eacute;lai maximum de 30 jours.<br />
Aucun remboursement ne sera effectu&eacute; en argent r&eacute;el (Euro, Dollar US, Dollar CA &hellip;)</p>

<p><strong>10- Donn&eacute;es &agrave; caract&egrave;re personnel</strong><br />
Conform&eacute;ment aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative &agrave; l&rsquo;informatique, aux fichiers et aux libert&eacute;s, tout Client dispose d&rsquo;un droit d&rsquo;acc&egrave;s, de rectification et d&rsquo;opposition aux donn&eacute;es personnelles le concernant, en effectuant sa demande &eacute;crite et sign&eacute;e, accompagn&eacute;e d&rsquo;une copie du titre d&rsquo;identit&eacute; avec signature du titulaire de la pi&egrave;ce, en pr&eacute;cisant l&rsquo;adresse &agrave; laquelle la r&eacute;ponse doit &ecirc;tre envoy&eacute;e.<br />
Le site BMS n&#39;est pas enregistr&eacute; &agrave; la CNIL.</p>

<p><strong>11- Droits</strong><br />
Le pr&eacute;sent contrat est soumis aux lois fran&ccedil;aises.</p>

<p><strong>12- Lexique</strong><br />
Site : Le Site d&eacute;signe le site Internet <a href="http://www.business-management-system.com/">BMS</a><br />
Client : Le Client est toute personne physique ou morale qui utilise le Site ou l&#39;un des services propos&eacute;s par BMS.<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
'
    ]);
  }
}
