<?php

use Illuminate\Database\Seeder;
use App\Users;

class SubscriptionsTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('subscriptions')->insert([
      'level' => '0',
      'starting_date' => new DateTime('2016-07-06'),
      'ending_date' => new DateTime('2017-07-05'),
      'id_users' => 1,
    ]);
    DB::table('subscriptions')->insert([
      'level' => '1',
      'starting_date' => new DateTime('2016-01-01'),
      'ending_date' => new DateTime('2016-12-31'),
      'id_users' => 2,
    ]);
  }
}
