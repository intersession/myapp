<?php

use Illuminate\Database\Seeder;
use App\Tables;
use App\Categories;
use App\Columns;

class ColumnsTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $faker = Faker\Factory::create();
    $categories = Categories::all()->lists('id')->All();

    DB::connection('mysql_api')->table('columns')->insert([
      'length' => '100',
      'type' => 'txt',
      'name' => 'Colonne 1',
      'id_tables' => 1,
      'id_categories' => $faker->randomElement($categories),
      'required' => true,
    ]);
    $this->createColumnDB(['name' => 'Colonne 1', 'type' => 'txt'], 1);

    DB::connection('mysql_api')->table('columns')->insert([
      'length' => '0',
      'type' => 'longtxt',
      'name' => 'Colonne 2',
      'id_tables' => 1,
      'required' => false,
    ]);
    $this->createColumnDB(['name' => 'Colonne 2', 'type' => 'longtxt'], 1);

    DB::connection('mysql_api')->table('columns')->insert([
      'length' => '0',
      'type' => 'float',
      'name' => 'Colonne 3',
      'id_tables' => 2,
      'id_categories' => $faker->randomElement($categories),
      'required' => true,
    ]);
    $this->createColumnDB(['name' => 'Colonne 3', 'type' => 'float'], 2);

    DB::connection('mysql_api')->table('columns')->insert([
      'length' => '0',
      'type' => 'mail',
      'name' => 'Colonne 4',
      'id_tables' => 3,
      'required' => false,
    ]);
    $this->createColumnDB(['name' => 'Colonne 4', 'type' => 'mail'], 3);
  }

  /**
  * Create the columns in myapp_customer
  * @param String $tableName
  */
  private function createColumnDB($array, $id)
  {
      $table = Tables::where('id', $id)->first();

      if($table){
          $tableName = $this->slugify($table->name);

          if (Schema::connection('mysql_customer')->hasTable($tableName)) {

              Schema::connection('mysql_customer')->table($tableName, function($table) use($array) {

                  switch ($array['type']) {
                      case 'txt':
                          $table->string($this->slugify($array['name']));
                          break;

                      case 'float':
                          $table->float($this->slugify($array['name']));
                          break;

                      case 'longtxt':
                          $table->longText($this->slugify($array['name']));
                          break;

                      case 'mail':
                          $table->string($this->slugify($array['name']));
                          break;
                  }
              });

              $this->insertData($tableName, $this->slugify($array['name']));
          }
      }
  }

  /**
  *
  * Insert data in table
  */
  private function insertData($tableName, $column)
  {
      DB::connection('mysql_customer')->table($tableName)->insert([
          'created_at' => new \DateTime(),
          'updated_at' => new \DateTime(),
          $column => $column,
      ]);
  }

  /**
  * @param $string
  * @return mixed|string
  * Generate slug from given string
  *
  */
  private function slugify($string)
  {
      $string = preg_replace('~[^\pL\d]+~u', '_', $string);
      $string = iconv('utf-8', 'us-ascii//TRANSLIT', $string);
      $string = preg_replace('~[^-\w]+~', '', $string);
      $string = trim($string, '_');
      $string = preg_replace('~-+~', '_', $string);
      $string = strtolower($string);

      return $string;
  }
}
