<?php

use Illuminate\Database\Seeder;
use App\Users;
use App\Roles;

class RolesUsersTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('roles_users')->insert([
      'id_users' => 1,
      'id_roles' => 1,
    ]);
    DB::table('roles_users')->insert([
      'id_users' =>  2,
      'id_roles' => 2,
    ]);
    DB::table('roles_users')->insert([
      'id_users' => 1,
      'id_roles' => 2,
    ]);
    DB::table('roles_users')->insert([
      'id_users' => 2,
      'id_roles' => 1,
    ]);
  }
}
