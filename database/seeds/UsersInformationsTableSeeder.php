<?php

use Illuminate\Database\Seeder;
use App\Users;

class UsersInformationsTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('users_informations')->insert([
      'address' => '15 rue des Roses',
      'city' => 'Lyon',
      'postal_code' => '69006',
      'birthday' => new DateTime('1990-05-23'),
      'id_users' => 1,
    ]);
    DB::table('users_informations')->insert([
      'address' => '86 avenue du Char',
      'city' => 'Lyon',
      'postal_code' => '69001',
      'birthday' => new DateTime('1985-09-15'),
      'id_users' => 2,
    ]);
  }
}
