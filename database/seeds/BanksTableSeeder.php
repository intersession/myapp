<?php

use Illuminate\Database\Seeder;
use App\Subscriptions;

class BanksTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $faker = Faker\Factory::create();
    $subscriptions = Subscriptions::all()->lists('id')->All();

    DB::table('banks')->insert([
      'iban' => 'CPCCBBANBBANBBANBBANBBANBBANBB',
      'bic' => 'LLLLLLXXXXX',
      'name' => 'BankOne',
      'address' => '45 rue de la peur',
      'city' => 'Lyon',
      'postal_code' => '69005',
      'id_subscriptions' => $faker->randomElement($subscriptions),
    ]);
    DB::table('banks')->insert([
      'iban' => 'CPCCBBANBBANBBANBBANBBANBBANBB',
      'bic' => 'LLLLLLXXXXX',
      'name' => 'BankTwo',
      'address' => "456 avenue de l'Europe",
      'city' => 'Lyon',
      'postal_code' => '69009',
      'id_subscriptions' => $faker->randomElement($subscriptions),
    ]);
  }
}
