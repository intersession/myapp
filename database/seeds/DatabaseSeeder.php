<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // user
        $this->call(CompaniesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RolesUsersTableSeeder::class);//

        // back
        $this->call(UsersInformationsTableSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(SubscriptionsTableSeeder::class);
        $this->call(BanksTableSeeder::class);//

        // front
        $this->call(CategoriesTableSeeder::class);
        $this->call(EntitiesTableSeeder::class);
        $this->call(TablesTableSeeder::class);
        $this->call(ColumnsTableSeeder::class);
        $this->call(PagesTableSeeder::class);

        Model::reguard();
    }
}
