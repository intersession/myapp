<?php

use Illuminate\Database\Seeder;
use App\Roles;

class RolesTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('roles')->insert([
      'name' => Roles::ADMIN,
      'id' => 1
    ]);
    DB::table('roles')->insert([
      'name' => Roles::SUPERADMIN,
      'id' => 2
    ]);
  }
}
