<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::connection('mysql_api')->table('categories')->insert([
      'name' => 'CategorieOne',
      'description' => 'Ceci est une description un.',
    ]);
    DB::connection('mysql_api')->table('categories')->insert([
      'name' => 'CategorieTwo',
      'description' => 'Ceci est une description deux.',
    ]);
  }
}
