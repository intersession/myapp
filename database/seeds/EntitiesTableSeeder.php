<?php

use Illuminate\Database\Seeder;
use App\Categories;

class EntitiesTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $faker = Faker\Factory::create();
    $categories = Categories::all()->lists('id')->All();

    DB::connection('mysql_api')->table('entities')->insert([
      'name' => 'EntityOne',
      'id_categories' => $faker->randomElement($categories),
    ]);
    DB::connection('mysql_api')->table('entities')->insert([
      'name' => 'EntityTwo',
      'id_categories' => $faker->randomElement($categories),
    ]);
    DB::connection('mysql_api')->table('entities')->insert([
      'name' => 'EntityThree',
      'id_categories' => $faker->randomElement($categories),
    ]);
    DB::connection('mysql_api')->table('entities')->insert([
      'name' => 'EntityFour',
      'id_categories' => $faker->randomElement($categories),
    ]);
  }
}
