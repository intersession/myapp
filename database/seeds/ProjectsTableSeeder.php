<?php

use Illuminate\Database\Seeder;
use App\Users;

class ProjectsTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    DB::table('projects')->insert([
      'slug' => 'one',
      'bdd_password' => bcrypt('secret'),
      'name' => 'ProjectOne',
      'description' => 'Ceci est une description du premier projet.',
      'id_users' => 1,
      'id' => 1
    ]);
  }
}
