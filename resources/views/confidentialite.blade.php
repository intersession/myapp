@extends('layouts.site-layout')

@section('title', 'Confidentialités')

@section('content')
    <main style="margin-top: 128px">
    <div class="container presentation" style="padding-top:20px;">
        <div class="row" id="confidential">
        <h1>Paramètres de confidentialité</h1>

        <p><strong>Que sont des « informations personnelles »</strong><br />
            L'information est considérée comme personnelle quand elle dit quelque chose au sujet d'un être humain qui est ou peut être (uniquement) identifié.
            Cette définition provient du droit européen, qui s'applique à BMS, et est intentionnellement large afin de fournir un niveau élevé de protection de la vie privée.<br/>
            Cela signifie, par exemple, que non seulement les noms et adresses email peuvent être des informations personnelles, mais aussi des numéros et autres identificateurs, tels que votre adresse IP, dans la mesure où ils relient d'autres informations à un être humain spécifique.
        </p>
        <p><strong>Adresses IP</strong><br />
            L'adresse IP qui est associée à votre recherche ne sera pas enregistrée ou partagée.
            Afin d'éviter la désagréable possibilité que quelque chose que vous recherchez en ligne puisse être retracé jusqu'à vous via votre adresse IP, nous n'enregistrons ni ne partageons l'adresse IP d'aucune de nos visiteurs.
            <br/>
            La seule exception est pour les demandes « abusives » (automatisées) qui soumettent rapidement plus de requêtes à BMS dans un court laps de temps que ce que pourrait faire tout être humain normal.
        </p>

        <p><strong>Recueil des informations</strong><br />
            Nous ne collectons aucune information personnelle sur nos visiteurs.
            Ceci est une condition primordiale et très importante pour protéger votre vie privée. Si les informations personnelles ne sont pas collectées, elles ne peuvent pas être volées, exigées, fuitées ou exposées.
            <br/>
            En fait, la seule information que nous enregistrons est l'ensemble des recherches effectuées sur notre site chaque jour (une mesure du trafic global) et ponctuellement, d'autres statistiques strictement non-personnelles. Ces statistiques peuvent inclure le nombre de fois où notre service est utilisé par un type de système d'exploitation, un type de navigateur, une langue, etc. ; ou encore une combinaison de ces critères.
            <br/>
            Nous ne comptons que les chiffres et ne sommes en aucune façon en mesure de savoir ce que peuvent être votre système d'exploitation spécifique, votre navigateur, vos paramètres de langue, etc., parce que nous n'enregistrons pas cette information.
            <br/>
            Lorsque vous utilisez BMS, nous n'enregistrons ni votre adresse IP, ni le navigateur associé à votre recherche (Internet Explorer, Safari, Firefox, Chrome, etc.), ni le système d'exploitation de l'ordinateur (Windows, Mac, Linux, etc.) associé à votre recherche, ni même les termes ou les phrases recherchés eux-mêmes.
            <br/>
            Cette politique de zéro collecte de données à caractère personnel protège votre vie privée, puisque votre adresse IP, votre navigateur, et vos informations de plate-forme peuvent être combinées avec d'autres données pour identifier de manière unique votre ordinateur, votre emplacement, ou vous-même.
            <br/>
            Veuillez noter que les requêtes de recherche peuvent transmettre des informations personnelles (pensez à quelqu'un qui entre son propre nom et/ou numéro de sécurité sociale). Bien que nous ne comptabilisions vos requêtes de recherche, toutes les requêtes sont soumises à Google de façon anonyme, car cela est nécessaire pour obtenir des résultats de recherche.
        </p>

        <p><strong>Cookies</strong><br />
            BMS n'utilise pas de cookies d'identification.
            Les fichiers témoins sont des ensembles de données transmis et enregistrés sur l’unité de disque dur de votre ordinateur lorsque vous consultez un site. BMS n’utilise qu’un seul fichier témoin - anonyme – appelé « préférences ». Ce fichier permet de conserver en mémoire les préférences que vous avez enregistrées pour votre prochaine visite. Il vient à expiration après 90 jours.
            <br/>
            Vous pouvez choisir d’accepter ou de refuser des fichiers témoins en modifiant la configuration de votre navigateur. Il importe toutefois de remarquer que vos préférences BMS ne fonctionnent pas correctement si vos fichiers témoins sont inactivés. Il est possible de n’accepter des fichiers témoins qu’en provenance de certains sites choisis.
            <br/>
            Vous pouvez supprimer régulièrement les cookies du disque dur de votre ordinateur, effacer toutes les données d'historique qui peuvent avoir été enregistrées à partir de votre navigation précédente sur le web. Si vous supprimez les cookies et revenez sur un site web, il n'y a pas de cookie pour vous reconnaître.
            <br/>
        </p>

        <p><strong>Adresses e-mail</strong><br />
            BMS ne vendra et ne divulguera vos coordonnées à aucune tierce partie.<br/>
            Lorsque vous envoyez des commentaires, des questions, des suggestions ou des retours à BMS, que ce soit par e-mail ou par le biais de notre centre d’assistance, et que vous y joignez une adresse e-mail et/ou des coordonnées, BMS peut utiliser ces informations pour vous répondre. En nous envoyant des commentaires, vous nous autorisez également à détecter le navigateur et le système d’exploitation de votre machine. Cela nous permet de mieux cerner les problèmes que vous rencontrez et de les résoudre plus efficacement. BMS ne révèlera ni ne revendra vos coordonnées à des tiers. En revanche, en de très rares occasions, il nous arrivera d’envoyer des e-mails à nos utilisateurs pour les tenir informés de l’évolution d’BMS en tant qu’entreprise. Ce service est disponible sur inscription uniquement et vous pouvez vous désabonner à tout moment.
        </p>

        <p><strong>Liens Externes</strong><br />
            Après avoir cliqué sur un résultat de recherche, vous quittez la protection offerte par BMS, sauf si vous utilisez notre fonction unique de proxy pour protéger votre identité.
            Veuillez noter que cette politique de confidentialité ne concerne que le site BMS.com. BMS est un moteur de recherche, et un éditeur de publicité, de sorte que vous trouverez différents liens sur notre site, y compris mais non limité à, des résultats de recherche et des résultats de recherche sponsorisés. Ces sites sont détenus et gérés par des sociétés indépendantes, sur lesquelles BMS n'a aucun contrôle, et qui ne sont pas couverts par cette politique de confidentialité. BMS n'est pas responsable du contenu de ces sites, ou de leur collecte et utilisation des informations.
            <br/>
            BMS empêche la fuite de vos termes de recherche à des tiers en utilisant la méthode dite POST par défaut au lieu de la méthode GET, qui est utilisé par de nombreux autres fournisseurs de recherche. La méthode POST garde vos termes de recherche en dehors des journaux de webmasters des sites que vous atteignez depuis nos résultats. Les termes de recherche en disent long sur ce que vous pensez, ce qui explique pourquoi c'est une question de confidentialité. Avec la méthode POST qu'BMS utilise, vos termes de recherche sont effacées. Cela peut provoquer des messages « la page web a expiré » et empêche de créer des raccourcis de pages de résultats, mais est préférable du point de vue de la confidentialité. Pour créer des raccourcis, vous pouvez utiliser le bouton « Ajouter cette recherche comme favori » sur la droite de la page de résultats. Sinon, vous pouvez désactiver « Utiliser POST plutôt que GET » complètement dans les « Réglages ».
            <br/>
            BMS offre également une fonction de proxy qui permet aux utilisateurs de voir les informations des sites externes sans divulguer leur adresse IP à ces sites.
        <p>

        <p><strong>Diffusion des informations</strong><br />
            BMS ne partage pas d'informations personnelles avec aucun tiers, y compris son ou ses fournisseur(s) de résultats de recherche réguliers ou sponsorisés.
            Lorsque vous effectuez une recherche sur BMS, l'expression de recherche doit être envoyée au(x) fournisseur(s) de résultats de la recherche, de sorte qu'ils puissent être générés et vous être présentés sur le site BMS.
            <br/>
            Quelques pages de résultats sur BMS comprennent un petit nombre de liens sponsorisés clairement étiquetés pour générer des revenus et couvrir les coûts opérationnels. Ces liens sont récupérés à partir de plates-formes comme Google Adwords. Parce que nous n'établissons pas votre profil, les publicités ne sont pas adaptées individuellement aux utilisateurs. Afin de permettre la prévention de la fraude au clic, des informations système non-identifiantes sont transmises au(x) fournisseur(s) de résultats parrainés. Nous ne partageons jamais votre adresse IP ou d'autres données qui peuvent vous identifier de façon unique.
            <br/>
            Aucun cookie de suivi n'est utilisé, donc il n'y a pas de cookie d'informations à partager.
        </p>

        <p><strong>Le droit à l’oubli</strong><br />
            Sous réserve de certaines conditions, les citoyens de l’UE ont le droit de demander la suppression de leurs données personnelles lorsque celles-ci s’avèrent inappropriées, hors de propos, diffamatoires ou qu’elles ne sont plus pertinentes. Pour plus d’informations,
            <a href="#">cliquez ici</a>.
        </p>
        </div>
    </div>
</main>
@stop