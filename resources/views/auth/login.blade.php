<!-- resources/views/auth/login.blade.php -->


@extends('layouts.site-layout')

@section('content')
    <div class="container">
            <div class="row">
                <div class="col m12" style="text-align: center">
                    <img src="{!! URL::asset('img/logo-intersession.png') !!}"  alt="logo" title="logo" class="logo"/>
                </div>
            </div>
            <form method="POST" action="/api/v1/auth/login">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col s12 offset-m4 m4">
                        <div class="row">
                            <div class="input-field col s12">
                                <h3>Connexion</h3>
                            </div>
                        </div>
                        {!! Form::hidden('type', 'local', ['class' => 'form-control', 'placeholder' => 'Email *']) !!}
                        <div class="row">
                            <div class="input-field col s12 ">
                                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email *']) !!}
                                {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password *']) !!}
                                {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <a href="#">Mot de passe oublié</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                {!! Form::submit('Se connecter', ['class' => 'waves-effect waves-light btn-large pull-right button']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </form>
    </div>
