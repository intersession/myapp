@extends('layouts.site-layout')

@section('title', 'Conditions Générales de Vente')

@section('content')


    <div class="container presentation" style="padding-top:20px;">
    	<div class="row" id="cgv">
    		{!! $cgv->content !!}
    	</div>
	   
	</div>

@stop