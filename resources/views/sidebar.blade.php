

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div>
                <p style="color:white; font-size:25px;">John Doe</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> En ligne</a>
            </div>
        </div>



        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENUS</li>
            <li class="treeview">
                <a href="/admin/#"><span>Statistiques Site</span> 
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/stats/D">Aujourd'hui</a></li>
                    <li><a href="/admin/stats/W">Semaine</a></li>
                    <li><a href="/admin/stats/M">Mois</a></li>
                    <li><a href="/admin/stats/Y">Année</a></li>
                </ul>
            </li>
            <li><a href="/admin/user"><span>Gestion des utilisateurs</span></a></li>
            <li><a href="/admin/pages"><span>Gestion des pages</span></a></li>
            <li><a href="/admin/abonnements"><span>Gestion des abonnements</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
