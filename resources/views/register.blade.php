
@extends('layouts.site-layout')

@section('content')
<style>
.input-field input:focus {
   border-bottom: 1px solid #e84627 !important;
 }
.titre{
   background-color: #E84627;
   text-align: center;
   color:white;
 }
</style>
    <div class="row">
        <div class="col m12" style="text-align:center">
            <img src="{{ URL::asset('img/logo-intersession.png') }}"  alt="logo" title="logo" class="logo"/>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col m12">
          <div class="banniere" style=" background:url(img/SEO.jpg);margin-top:60px;">
              <div class="headerText container">
                  <h1 style="color:#333; font-weight:500; visibility: visible; ">You're 30 Seconds Away From Your Free Trial</h1>
                  <p style="color:#aaa;visibility: visible;text-align:left !important;">You're 30 Seconds Away From Your Free Trial
                    You're 30 Seconds Away From Your Free Trial<br>You're 30 Seconds Away From Your Free Trial
                      You're 30 Seconds Away From Your Free Trial</p>
              </div>
          </div>
        </div>
    </div>

{!! Form::open(array('url' => 'register', 'method' => 'POST')) !!}
<div class="row ">
   <div class="col m4 offset-m2 s12 formulaire">
       <div class="row">
           <div class="input-field col s12 ">
               <h3>Inscription</h3>
           </div>
       </div>
       <div class="row">
           <div class="input-field col s12">
               <i class="material-icons prefix" style="color: #E84627 !important;">account_circle</i>
               {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Nom']) !!}
               {!! $errors->first('lastname', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 ">
                        <i class="material-icons prefix" style="color: #E84627 !important;">account_circle</i>
                        {!! Form::text('firstname', null, ['class' => 'form-control', 'placeholder' => 'Prénom']) !!}
                        {!! $errors->first('firstname', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 ">
                        <i class="material-icons prefix" style="color: #E84627 !important;">email</i>
                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                        {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 ">
                        <i class="material-icons prefix" style="color: #E84627 !important;">lock</i>
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Mot de passe']) !!}
                        {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 ">
                        <i class="material-icons prefix" style="color: #E84627 !important;">lock</i>
                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmation mot de passe']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                      <i class="material-icons prefix" style="color: #E84627 !important;">phone</i>
                        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Tel']) !!}
                        {!! $errors->first('phone', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 ">
                      <i class="material-icons prefix" style="color: #E84627 !important;">business</i>
                        {!! Form::text('company', null, ['class' => 'form-control', 'placeholder' => 'Company']) !!}
                        {!! $errors->first('company', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                      <i class="material-icons prefix" style="color: #E84627 !important;">work</i>
                        {!! Form::text('job', null, ['class' => 'form-control', 'placeholder' => 'Job title']) !!}
                        {!! $errors->first('job', '<small class="help-block">:message</small>') !!}
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s5">
                        {!! Form::submit('Envoyer', ['class' => ' btn-large button']) !!}
                    </div>
                </div>
            </div>
            <div class="col s5 offset-s1" style="margin-top:50px;">
                <div class="row">
                    <div class="row">
                        <div class="input-field col s12">
                            <p style="text-align:left !important;"><b>ONLINE DATABASE</b></p>
                            <p style="text-align:left !important;">Built-in database that is both powerful and user-friendly.</p>
                            <p style="text-align:left !important;"><b>WEB APPLICATION BUILDER</b></p>
                            <p style="text-align:left !important;"> Point-and-click wizards for creating business applications without coding.</p>
                            <p style="text-align:left !important;"><b>CLOUD INFRASTRUCTURE</b></p>
                            <p style="text-align:left !important;">  A reliable, secure, and scalable cloud infrastructure on AWS to power your applications.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
