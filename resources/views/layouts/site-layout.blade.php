<!-- Stored in resources/views/layouts/site-layout.blade.php -->

<html>
<head>
    <title>BMS | @yield('title')</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link href="{{ URL::asset('css/materialize.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/materialize.min.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('js/jquery-3.0.0.js') }}"></script>
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{{ URL::asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/owl.transitions.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/inscription.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/presentation.css') }}">
    {{-- sequence --}}
    <link rel="stylesheet" href="{{ URL::asset('css/sequence/sequence.css') }}" media="all">

    <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>

<div>
    <header>
        <nav class="light-blue lighten-1" role="navigation">
            <div class="nav-wrapper container"><a id="logo-container" href="/" class="brand-logo">{!! Html::image('img/logo-intersession.png', 'Business Manager System') !!}</a>
                <ul class="right hide-on-med-and-down" id="presentation-nav">
                    <li></li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li><a href="../register" class="btn waves-effect waves-light orange">S'inscrire</a></li>
                </ul>
                {{--<ul class="right hide-on-med-and-down">--}}
                    {{--<li><a class="btn waves-effect waves-light orange btn_connexion" href="#">Se connecter</a></li>--}}

                    {{--<li>{!! Html::linkRoute('register', 'S\'inscrire', array(), array('class' => 'btn waves-effect waves-light orange')) !!}</li>--}}
                {{--</ul>--}}
                <ul class="right hide-on-med-and-down">
                    <li>{!! Html::linkRoute('api_login_angular', 'Se connecter', array(), array('class' => 'btn waves-effect waves-light orange')) !!}</li>
                </ul>
                <ul id="nav-mobile" class="side-nav">
                    <li><a href="../register">S'inscrire</a></li>
                    <li>{!! Html::linkRoute('api_login_angular', 'Se connecter', array(), array('class' => 'btn waves-effect waves-light orange')) !!}</li>
                </ul>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
            </div>
        </nav>
    </header>
    <main style="margin-top: 128px">
    @yield('content')
    </main>
    <footer class="page-footer grey">
        <div class="container">
            <div class="row">
                <div class="col l3 s12 img-responsive-50">
                    <a id="logo-footer" href="/" class="brand-logo">{!! Html::image('img/logo-intersession.png', 'BMS - Business System Manager') !!}</a>
                </div>

                <div class="col l9 s12 img-right">
                    <a id="logo-docker" href="#" class="brand-logo">{!! Html::image('img/Docker.png', 'BMS - Docker Partner', array( 'class' => 'footer-image', 'height' => 82 )) !!}</a>
                    <a id="logo-amazon" href="#" class="brand-logo">{!! Html::image('img/Amazon.png', 'BMS - Amazon Partner', array( 'class' => 'footer-image', 'height' => 82 )) !!}</a>
                    <a id="logo-linux" href="#" class="brand-logo">{!! Html::image('img/Linux.png', 'BMS - Linux Partner', array( 'class' => 'footer-image', 'height' => 82 )) !!}</a>
                    <a id="logo-cloud" href="#" class="brand-logo">{!! Html::image('img/Cloudfare.png', 'BMS - Cloudfare Partner', array( 'class' => 'footer-image', 'height' => 82 )) !!}</a>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container right-align">
                Made by <a class="orange-text text-lighten-3" href="http://www.ecole-ipssi.com/">Ipssi Team</a> |
                {!! Html::linkRoute('mentions-legales', 'Mentions Légales', array(), array('class' => 'black-text text-lighten-3')) !!} |
                {!! Html::linkRoute('cgv', 'CGV', array(), array('class' => 'black-text text-lighten-3')) !!} |
                {!! Html::linkRoute('confidentialite', 'Confidentialité', array(), array('class' => 'black-text text-lighten-3')) !!}
            </div>
        </div>
    </footer>
</div>
    {{--Start Sequence JS--}}
        <script src="{{ URL::asset('js/sequence/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ URL::asset('js/sequence/hammer.min.js') }}"></script>
        <script src="{{ URL::asset('js/sequence/sequence.min.js') }}"></script>
        <script src="{{ URL::asset('js/sequence/sequence-theme.intro.js') }}"></script>
    {{--End Sequence JS--}}

    <!--Import jQuery before materialize.js-->
    <script src="{{ URL::asset('js/owl.carousel.js') }}"></script>
    <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ URL::asset('js/materialize.js') }}"></script>
    <script src="{{ URL::asset('js/materialize.min.js') }}"></script>
    <script src="{!! URL::asset('js/sweetalert.min.js') !!}"></script>
    <script src="{!! URL::asset('js/sweetalert-dev.js') !!}"></script>
    <script src="{{ URL::asset('js/init.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.btn_connexion').click(function() {
            $('#modal1').openModal();
            });
            $("#owl-landing").owlCarousel({

                // Most important owl features
                items : 6,
                itemsCustom : false,
                itemsDesktop : [1199,6],
                itemsDesktopSmall : [980,3],
                itemsTablet: [768,2],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                singleItem : false,
                itemsScaleUp : false,

                //Basic Speeds
                slideSpeed : 200,
                paginationSpeed : 800,
                rewindSpeed : 1000,

                //Autoplay
                autoPlay : false,
                stopOnHover : false,

                // Navigation
                navigation : false,
                navigationText : ["prev","next"],
                rewindNav : true,
                scrollPerPage : false,

                //Pagination
                pagination : true,
                paginationNumbers: false,

                // Responsive
                responsive: true,
                responsiveRefreshRate : 200,
                responsiveBaseWidth: window,

                // CSS Styles
                baseClass : "owl-carousel",
                theme : "owl-theme",

                //Lazy load
                lazyLoad : false,
                lazyFollow : true,
                lazyEffect : "fade",

                //Auto height
                autoHeight : false,

                //JSON
                jsonPath : false,
                jsonSuccess : false,

                //Mouse Events
                dragBeforeAnimFinish : true,
                mouseDrag : true,
                touchDrag : true,

                //Transitions
                transitionStyle : false,

                // Other
                addClassActive : false,

                //Callbacks
                beforeUpdate : false,
                afterUpdate : false,
                beforeInit: false,
                afterInit: false,
                beforeMove: false,
                afterMove: false,
                afterAction: false,
                startDragging : false,
                afterLazyLoad : false

            });
        });
        // Carousel de la page présentation
        $("#owl-presentation").owlCarousel({

            navigation : false, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            loop:true,
            margin:0,
            pagination: false,
            autoPlay:true,
            stopOnHover:true

            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false

        });
    </script>
    <div id="modal1" class="modal modal-fixed-footer" style="width:20%;max-height:500px;">
      <div class="modal-content">

        <form method="POST" action="/api/v1/auth/login">
            {!! csrf_field() !!}
            <div class="container-fluid">
                  <div class="row">
                    <div class="col m12" style="background-color: #E84627;text-align: center;color:white;margin-top:none !important;">
                        <h4>Connexion</h4>
                  </div>
              </div>
              <div class="col m12">
                  <div class="row">
                      <div class="input-field col m12 ">
                          {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email *']) !!}
                          {!! $errors->first('email', '<small class="help-block">:message</small>') !!}
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col m12">
                          {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password *']) !!}
                          {!! $errors->first('password', '<small class="help-block">:message</small>') !!}
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col m12">
                          <a href="#">Mot de passe oublié</a>
                      </div>
                  </div>
                  <div class="row">
                      <div class="input-field col m12">
                          {!! Form::submit('Se connecter', ['class' => ' btn-large button right']) !!}
                      </div>
                  </div>
              </div>
            </div>
            </div>
        </form>
      </div>
      {{--<div class="modal-footer">--}}
        {{--<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Connexion</a>--}}
      {{--</div>--}}
    </div>
</body>
</html>
