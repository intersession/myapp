<!-- Stored in resources/views/layouts/site-layout.blade.php -->

<html>
<head>
    <title>BMS | @yield('title')</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link href="{!! URL::asset('css/materialize.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('css/materialize.min.css') !!}" rel="stylesheet">

    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{!! URL::asset('css/owl.carousel.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('css/owl.theme.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('css/owl.transitions.css') !!}">
    <link rel="stylesheet" href="{!! URL::asset('css/inscription.css') !!}">
    <link href="{!! URL::asset('css/style.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('css/main.css') !!}" rel="stylesheet"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>

<div>

    @yield('content')
    <footer class="page-footer grey">
        <div class="container">
            <div class="row">
                <div class="col l3 s12 img-responsive-50">
                    <a id="logo-container" href="#" class="brand-logo">{!! HTML::image('img/logo-intersession.png', 'BMS - Business System Manager') !!}</a>
                </div>

                <div class="col l9 s12 img-right">
                    <a id="logo-container" href="#" class="brand-logo">{!! HTML::image('img/Docker.png', 'BMS - Docker Partner', array( 'width' => 164, 'height' => 82 )) !!}</a>
                    <a id="logo-container" href="#" class="brand-logo">{!! HTML::image('img/Amazon.png', 'BMS - Amazon Partner', array( 'width' => 82, 'height' => 82 )) !!}</a>
                    <a id="logo-container" href="#" class="brand-logo">{!! HTML::image('img/Linux.png', 'BMS - Linux Partner', array( 'width' => 82, 'height' => 82 )) !!}</a>
                    <a id="logo-container" href="#" class="brand-logo">{!! HTML::image('img/Cloudfare.png', 'BMS - Cloudfare Partner', array( 'width' => 120, 'height' => 82 )) !!}</a>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container right-align">
                Made by <a class="orange-text text-lighten-3" href="http://www.ecole-ipssi.com/">Ipssi Team</a> |
                {!! HTML::linkRoute('mentions-legales', 'Mentions Légales', array(), array('class' => 'black-text text-lighten-3')) !!} |
                {!! HTML::linkRoute('confidentialite', 'Confidentialité', array(), array('class' => 'black-text text-lighten-3')) !!}
            </div>
        </div>
    </footer>
</div>
<!--Import jQuery before materialize.js-->
<script src="{!! URL::asset('js/sweetalert.min.js') !!}"></script>
<script src="{!! URL::asset('js/sweetalert-dev.js') !!}"></script>
<script src="{!! URL::asset('js/jquery-3.0.0.js') !!}"></script>
<script src="{!! URL::asset('js/owl.carousel.js') !!}"></script>
<script src="{!! URL::asset('js/owl.carousel.min.js') !!}"></script>
<script src="{!! URL::asset('js/materialize.js') !!}"></script>
<script src="{!! URL::asset('js/materialize.min.js') !!}"></script>
<script src="{!! URL::asset('js/init.js') !!}"></script>

</body>
</html>