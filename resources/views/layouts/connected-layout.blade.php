<!-- Stored in resources/views/layouts/site-layout.blade.php -->

<html>
<head>
    <title>BMS | @yield('title')</title>
    <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
@section('sidebar')
    This is the master sidebar.
@show

<div class="container">
    @yield('content')
</div>
</body>
</html>