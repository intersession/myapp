<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ $page_title or "AdminLTE Dashboard" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ URL::asset("admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ URL::asset("admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />




    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css") }}"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/skins/_all-skins.min.css") }}"/>



</head>
<body class="skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('header')

        <!-- Sidebar -->
        @include('sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    {{ $page_title or "Statistics" }}
                    <small>{{ $page_description or null }}</small>
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
               <fieldset>
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="selectbasic">Période</label>
                        <div class="col-md-4">
                            <select id="selectbasic" name="selectbasic" class="form-control" onchange="Reload(this.value)">
                                <option value="D">Aujourd'hui</option>
                                <option value="W">Semaine</option>
                                <option value="M">Mois</option>
                                <option value="Y">Année</option>
                            </select>
                        </div>
                    </div>
                </fieldset><br/>

            <!-- DONUT CHART -->
            <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Number of visits</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <canvas id="pieChart" style="height:150px !important;"></canvas>
                </div>
            </div>


              <?php if(count($totalVisitsViews) == 1) { ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="col-lg-3 col-xs-6">
                          <div class="small-box bg-blue">
                            <div class="inner">
                                <h3><?php echo $totalVisitsViews[0]['visits']; ?></h3>
                                <h2>Visits</h2>
                                <p><?php echo $totalVisitsViews[0]['date']; ?></p>
                            </div>
                            <div class="icon">
                              <i class="ion ion-person"></i>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                          <div class="small-box bg-red">
                            <div class="inner">
                                <h3><?php echo $totalVisitsViews[0]['views']; ?></h3>
                                <h2>Views</h2>
                                <p><?php echo $totalVisitsViews[0]['date']; ?></p>
                            </div>
                            <div class="icon">
                              <i class="ion ion-eye"></i>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                          <div class="small-box bg-green">
                            <div class="inner">
                              <h3><?php echo $bounceRate[0]['bounceRate']; ?><sup style="font-size: 20px">%</sup></h3>
                              <h2>Bounce Rate</h2>
                              <p><?php echo $bounceRate[0]['date']; ?></p>
                            </div>
                            <div class="icon">
                              <i class="ion ion-arrow-graph-up-right"></i>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-purple">
                                <div class="inner">
                                    <h3><?php echo $avgTimeOnSite[0]['avg']; ?></h3>
                                    <h2>Time on Site</h2>
                                    <p><?php echo $avgTimeOnSite[0]['date']; ?></p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-clock"></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            <?php } if(count($totalVisitsViews) > 1) { ?>
                
                <!-- Views and visits -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">Views and Visits</h3>
                    </div>
                    <div class="box-body">
                      <div class="chart">
                        <canvas id="areaChart" style="height:250px"></canvas>
                      </div>
                    </div>
                </div>


              <!-- Bounce Rates -->
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">BounceRates</h3>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="bounceRates" style="height:250px"></canvas>
                  </div>
                </div>
              </div>


              <!-- AVG Time On Site -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Time On Site - <i>En minutes</i></h3>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="TimeOnSite" style="height:250px"></canvas>
                  </div>
                </div>
            </div>

            <?php } ?>


            <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">TOP 5 top visits</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered">
                    <tr>
                      <th>URL</th>
                      <th>Views</th>
                      <th>Visits</th>
                    </tr>

                    @foreach($topVisits as $v)
                        <tr>
                            <td>{{ $v['url'] }}</td>
                            <td>{{ $v['views'] }}</td>
                            <td>{{ $v['visits'] }}</td>
                        </tr>
                    @endforeach
                  </table>
                </div>
              </div>

            </section>
        </div>

        <!-- Footer -->
        @include('footer')

    </div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.3 -->
<script src="{{ URL::asset ("admin-lte/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ URL::asset ("admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset ("admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{ URL::asset ("admin-lte/plugins/chartjs/Chart.min.js") }}"></script>
<!-- FastClick -->
<script src="{{ URL::asset ("admin-lte/plugins/fastclick/fastclick.js") }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ URL::asset ("admin-lte/dist/js/demo.js") }}"></script>




<script>

    function Reload(filtre) 
    {
        document.location.href = "/admin/stats/" + filtre;
    }

    $(function () {

    //-------------
    //- DEVICES -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.


    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [
      {
        value: {{ $visitsDevices['mobile'] }},
        color: "#f56954",
        highlight: "#f56954",
        label: "Mobiles"
      },
      {
        value: {{ $visitsDevices['tablet'] }},
        color: "#00a65a",
        highlight: "#00a65a",
        label: "Tablets"
      },
      {
        value: {{ $visitsDevices['desktop'] }},
        color: "#f39c12",
        highlight: "#f39c12",
        label: "Desktop"
      }
    ];
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

    //--------------
    //- VISITS AND VIEWS -
    //--------------

    <?php if(count($totalVisitsViews) > 1) { ?>

        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var areaChart = new Chart(areaChartCanvas);
        
        var labels = [];
        var data1 = [];
        var data2 = [];

        <?php foreach($totalVisitsViews as $v) : ?>
            labels.push("<?php echo $v['date']; ?>");
            data1.push(<?php echo $v['visits']; ?>);
            data2.push(<?php echo $v['views']; ?>);
        <?php endforeach; ?>


        var areaChartData = {
            

          labels: labels,
          datasets: [
            {
              label: "Views",
              fillColor: "rgba(210, 214, 222, 1)",
              strokeColor: "rgba(210, 214, 222, 1)",
              pointColor: "rgba(210, 214, 222, 1)",
              pointStrokeColor: "#c1c7d1",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(220,220,220,1)",
              data: data2
            },
            {
              label: "Visits",
              fillColor: "rgba(60,141,188,0.9)",
              strokeColor: "rgba(60,141,188,0.8)",
              pointColor: "#3b8bba",
              pointStrokeColor: "rgba(60,141,188,1)",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "rgba(60,141,188,1)",
              data: data1
            }
          ]
        };

        var areaChartOptions = {
          //Boolean - If we should show the scale at all
          showScale: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: false,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - Whether the line is curved between points
          bezierCurve: true,
          //Number - Tension of the bezier curve between points
          bezierCurveTension: 0.3,
          //Boolean - Whether to show a dot for each point
          pointDot: false,
          //Number - Radius of each point dot in pixels
          pointDotRadius: 4,
          //Number - Pixel width of point dot stroke
          pointDotStrokeWidth: 1,
          //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
          pointHitDetectionRadius: 20,
          //Boolean - Whether to show a stroke for datasets
          datasetStroke: true,
          //Number - Pixel width of dataset stroke
          datasetStrokeWidth: 2,
          //Boolean - Whether to fill the dataset with a color
          datasetFill: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true
        };

        //Create the line chart
        areaChart.Line(areaChartData, areaChartOptions);
    <?php } ?>



    //--------------
    //- BOUNCE RATE -
    //--------------


    <?php if(count($bounceRate) > 1) { ?>

        // Get context with jQuery - using jQuery's .get() method.
        var bounceRatesCanvas = $("#bounceRates").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var bounceRates = new Chart(bounceRatesCanvas);
        
        var labels = [];
        var data = [];

        <?php foreach($bounceRate as $b) : ?>
            labels.push("<?php echo $b['date']; ?>");
            data.push(<?php echo $b['bounceRate']; ?>);
        <?php endforeach; ?>


        var bounceRatesData = {
            

          labels: labels,
          datasets: [
            {
              label: "BounceRates",
              fillColor: "#f56954",
              strokeColor: "#f56954",
              pointColor: "#f56954",
              pointStrokeColor: "#f56954",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "#f56954",
              data: data
            }
          ]
        };

        var bounceRatesOptions = {
          //Boolean - If we should show the scale at all
          showScale: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: false,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - Whether the line is curved between points
          bezierCurve: true,
          //Number - Tension of the bezier curve between points
          bezierCurveTension: 0.3,
          //Boolean - Whether to show a dot for each point
          pointDot: false,
          //Number - Radius of each point dot in pixels
          pointDotRadius: 4,
          //Number - Pixel width of point dot stroke
          pointDotStrokeWidth: 1,
          //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
          pointHitDetectionRadius: 20,
          //Boolean - Whether to show a stroke for datasets
          datasetStroke: true,
          //Number - Pixel width of dataset stroke
          datasetStrokeWidth: 2,
          //Boolean - Whether to fill the dataset with a color
          datasetFill: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true
        };

        //Create the line chart
        bounceRates.Line(bounceRatesData, bounceRatesOptions);

    <?php } ?>


    //--------------
    //- TIME ON SITE -
    //--------------

    <?php if(count($avgTimeOnSite) > 1) { ?>

        // Get context with jQuery - using jQuery's .get() method.
        var TimeOnSiteCanvas = $("#TimeOnSite").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var TimeOnSite = new Chart(TimeOnSiteCanvas);
        
        var labels = [];
        var data = [];

        <?php foreach($avgTimeOnSite as $b) : ?>
            labels.push("<?php echo $b['date']; ?>");

            <?php

                $sec = 0;

                $moyenne = explode(':',$b['avg']);
                if(count($moyenne) == 3)
                {
                    $sec += $moyenne[0] * 3600;
                    $sec += $moyenne[1]*60;
                    $sec += $moyenne[2];

                    $min = round($sec /60,2) ;
                }
            ?>


            data.push("<?php echo $min; ?>");
        <?php endforeach; ?>


        var TimeOnSiteData = {

          labels: labels,
          datasets: [
            {
              label: "BounceRates",
              fillColor: "#00a65a",
              strokeColor: "#00a65a",
              pointColor: "#00a65a",
              pointStrokeColor: "#00a65a",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "#00a65a",
              data: data
            }
          ]
        };

        var TimeOnSiteOptions = {
          //Boolean - If we should show the scale at all
          showScale: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: false,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - Whether the line is curved between points
          bezierCurve: true,
          //Number - Tension of the bezier curve between points
          bezierCurveTension: 0.3,
          //Boolean - Whether to show a dot for each point
          pointDot: false,
          //Number - Radius of each point dot in pixels
          pointDotRadius: 4,
          //Number - Pixel width of point dot stroke
          pointDotStrokeWidth: 1,
          //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
          pointHitDetectionRadius: 20,
          //Boolean - Whether to show a stroke for datasets
          datasetStroke: true,
          //Number - Pixel width of dataset stroke
          datasetStrokeWidth: 2,
          //Boolean - Whether to fill the dataset with a color
          datasetFill: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: true,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true
        };

        //Create the line chart
        TimeOnSite.Line(TimeOnSiteData, TimeOnSiteOptions);

    <?php } ?>



    

    });

</script>
</body>
</html>
