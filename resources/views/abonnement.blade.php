@extends('layouts.site-layout')
@section('content')
<style>
p {
  text-align: center !important;
}
.btnSuivant{
  background-color:  #E84627;
  color :  #E84627;
}
.cadre{
  margin-top:10px;
}
input::-moz-placeholder{
  color: black;
}
.titre{
  background-color: #E84627;
  text-align: center;
  color:white;
}
#modal1, #modal2, #modal3{
  margin-top: 50px;
}
.modal{
  width:40%  !important;
}
.btn-floating
{
  background-color: #E84627 !important;
}
.input-field {
  margin-top: 0px !important;

}

</style>
<div class="row">
    <div class="col m12" style="text-align:center">
        <h2>Choissisez votre abonnement</h2>
    </div>
</div>
<br/>
<div class="row">
    <div class="col m12 " style="text-align: center;">
        <div class="col m6" style="margin-top:20px">
            <div class="blocAbonnement" >
                <h1 class="card-title Type" style="text-align: center;padding-top: 50px;font-size: 20px;font-weight: bold;">FREE</h1>
                <span class="card-title" style="text-align: center;font-size: 45px;font-weight: bold;">
                    0<sup>€</sup><br/>
                </span>
                <span class="card-title colorgrey" style="text-align: center;font-size: 20px;font-weight: bold;">
                    par mois
                </span>
                <div style="text-align: center;">
                 {!! Form::submit('Selectionner', ['class' => 'month btn-large btnOffre btnFree button']) !!}
                </div>
                <div class="LigneDetail">
                  Version gratuite
                </div>
                <p style="text-align: center"> Unlimited Users</p>
                <p style="text-align: center"> 3 DataPages</p>
                <p style="text-align: center"> 250 MB Data Transfer</p>
                <p style="text-align: center"> Community Support</p>
                <p style="text-align: center"> Core Features </p>
                <div class="wrapper">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   viewBox="0 0 98.5 98.5" enable-background="new 0 0 98.5 98.5" xml:space="preserve">
                  <path class="checkmark" fill="none" stroke-width="8" stroke-miterlimit="10" d="M81.7,17.8C73.5,9.3,62,4,49.2,4
                  C24.3,4,4,24.3,4,49.2s20.3,45.2,45.2,45.2s45.2-20.3,45.2-45.2c0-8.6-2.4-16.6-6.5-23.4l0,0L45.6,68.2L24.7,47.3"/>
                  </svg>
                </div>
            </div>
        </div>
        <div class="col m6" style="margin-top:20px">
            <div class="blocAbonnement" >
                <h1 class="card-title Type" style="text-align: center;padding-top: 50px;font-size: 20px;font-weight: bold;">PREMIUM</h1>
                <span class="card-title" style="text-align: center;font-size: 45px;font-weight: bold;">
                    250<sup>€</sup><br/>
                </span>
                <span class="card-title colorgrey" style="text-align: center;font-size: 20px;font-weight: bold;">
                    par mois
                </span>
                <div style="text-align: center">
                   {!! Form::submit('Selectionner', ['class' => 'month btn-large btnOffre button']) !!}
                </div>
                <div class="LigneDetail">
                  Version payante
                </div>
                <p style="text-align: center"> 50 DataPages</p>
                <p style="text-align: center"> Unmetered Data Transfer</p>
                <p style="text-align: center"> Live Business Support</p>
                <p style="text-align: center"> Standard Features</p>
                <p style="text-align: center"> 2 Hours Expert Sessions</p>
                <div class="wrapper">
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   viewBox="0 0 98.5 98.5" enable-background="new 0 0 98.5 98.5" xml:space="preserve">
                  <path class="checkmark" fill="none" stroke-width="8" stroke-miterlimit="10" d="M81.7,17.8C73.5,9.3,62,4,49.2,4
                  C24.3,4,4,24.3,4,49.2s20.3,45.2,45.2,45.2s45.2-20.3,45.2-45.2c0-8.6-2.4-16.6-6.5-23.4l0,0L45.6,68.2L24.7,47.3"/>
                  </svg>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col m12"  style="text-align: center">
        {!! Form::submit('Valider', ['class' => 'month btn-large button btnValider']) !!}
    </div>
</div>
<!--  Modal ETAPE 1 -->
  {!! Form::open(array('url' => 'abonnement', 'method' => 'POST','id' =>'SubmitForm')) !!}
  {!! Form::hidden('id_user', $user->id)!!}
    <input type="hidden" id="abonnement" name="abonnement" value="">


    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
          <div class="row">
              <div class="col m12 titre">
                <h4 style="margin-top:10px;">INFORMATIONS</h4>
              </div>
          </div>
          <div class="row">
            <div class="col m6">
              <h5><i class="small material-icons" style="color: #E84627;">perm_contact_calendar</i>Coordonnées</h5>
            </div>
            <div class="col m5">
            <h5><i class="small material-icons" style="color: #E84627;">my_location</i>Adresse de livraison</h5>
            </div>
          </div>
          <div class="row">
              <div class="col m4 input-field">
                <i class="material-icons prefix" style="color: #E84627 !important;">account_circle</i>
                <input type="text" id="Nom" name="Nom" value=<?php echo $user->lastname ?>>
              </div>
              <div class="col m1">
                <a class="waves-effect btn-floating"><i class="material-icons" >mode_edit</i></a>
              </div>
              <div class="col m5 offset-m1 input-field">
                <i class="material-icons prefix" style="color: #E84627 !important;">pin_drop</i>
                <input type="text" id="Rue" name="Rue" placeholder='Numéro de rue'/>
              </div>
          </div>
          <div class="row">
              <div class="col m4 input-field">
                  <i class="material-icons prefix" style="color: #E84627 !important;">account_circle</i>
                  <input type="text" id="Prenom" name="Prenom" value=<?php echo $user->firstname ?>>
              </div>
              <div class="col m1">
                <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
              </div>
              <div class="col m5 offset-m1 input-field">
                <i class="material-icons prefix" style="color: #E84627 !important;">track_changes</i>
                <input type="text" id="Adresse"  name="Adresse" placeholder='Adresse'/>
              </div>
          </div>
          <div class="row">
                <div class="col m4 input-field">
                  <i class="material-icons prefix" style="color: #E84627 !important;">email</i>
                  <input type="text" id="Email" name="Email" value=<?php echo $user->email ?>>
                </div>
                <div class="col m1">
                  <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                </div>
              <div class="col m5 offset-m1 input-field">
                <i class="material-icons prefix" style="color: #E84627 !important;">near_me</i>
                <input type="text" id="CP" name="CP" placeholder='Code Postal'/>
              </div>
          </div>
          <div class="row">
                <div class="col m4 input-field">
                  <i class="material-icons prefix" style="color: #E84627 !important;">phone</i>
                  <input type="text" id="Tel" name="Tel" value='012345678'/>
                </div>
                <div class="col m1">
                  <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                </div>
          </div>
          <div class="row">
                <div class="col m4 input-field">
                    <i class="material-icons prefix" style="color: #E84627 !important;">location_city</i>
                    <input type="text" id="Ville" value='Ville'/>
                </div>
                <div class="col m1">
                  <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                </div>
          </div>
          <div class="row">
              <div class="col m4 input-field">
                  <i class="material-icons prefix" style="color: #E84627 !important;">business</i>
                  <input type="text" id="Entreprise" value='Sharewood'/>
              </div>
              <div class="col m1">
                <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
              </div>
          </div>
          <div class="row">
              <div class="col m4 input-field">
                <i class="material-icons prefix" style="color: #E84627 !important;">work</i>
                  <input type="text" id="Metier" name="Metier" value='Developper'/>
              </div>
              <div class="col m1">
                <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
              </div>
          </div>
        <!-- <div class="row">
              <div class="col m5">
                {!! Form::submit('Modifier', ['class' => 'btn-large button']) !!}
  <div id="modal1" class="modal modal-fixed-footer">
      <div class="modal-content">
        <div class="row">
            <div class="col s12 titre">
              <h4 style="margin-top:10px;">INFORMATIONS</h4>
            </div>
        </div>
        <div class="row">
          <div class="col m6 s12">
            <h5><i class="small material-icons" style="color: #E84627;">perm_contact_calendar</i>Coordonnées</h5>
            <div class="row">
                <div class="col s10 input-field">
                  <i class="material-icons prefix" style="color: #E84627 !important;">account_circle</i>
                  <input type="text" id="Nom" name="Nom" value='Dujardin'/>
                </div>
                <div class="col s1">
                  <a class="waves-effect btn-floating"><i class="material-icons" >mode_edit</i></a>
                </div>
            </div>
            <div class="row">
                <div class="col s10 input-field">
                    <i class="material-icons prefix" style="color: #E84627 !important;">account_circle</i>
                    <input type="text" id="Prenom" name="Prenom" value='Jean'/>
                </div>
                <div class="col s1">
                  <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                </div>
            </div>
            <div class="row">
                  <div class="col s10 input-field">
                    <i class="material-icons prefix" style="color: #E84627 !important;">email</i>
                    <input type="text" id="Email" name="Email" value='jean@dujardin.com'/>
                  </div>
                  <div class="col s1">
                    <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                  </div>
            </div>
            <div class="row">
                  <div class="col s10 input-field">
                    <i class="material-icons prefix" style="color: #E84627 !important;">phone</i>
                    <input type="text" id="Tel" name="Tel" value='012345678'/>
                  </div>
                  <div class="col s1">
                    <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                  </div>
            </div>
            <div class="row">
                  <div class="col s10 input-field">
                      <i class="material-icons prefix" style="color: #E84627 !important;">location_city</i>
                      <input type="text" id="Ville" value='Ville'/>
                  </div>
                  <div class="col s1 ">
                    <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                  </div>
            </div>
            <div class="row">
                <div class="col s10 input-field">
                    <i class="material-icons prefix" style="color: #E84627 !important;">business</i>
                    <input type="text" id="Entreprise" value='Sharewood'/>
                </div>
                <div class="col s1">
                  <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                </div>
            </div>
            <div class="row">
                <div class="col s10 input-field">
                  <i class="material-icons prefix" style="color: #E84627 !important;">work</i>
                    <input type="text" id="Metier" name="Metier" value='Developper'/>
                </div>
                <div class="col s1">
                  <a class="waves-effect btn-floating"><i class="material-icons">mode_edit</i></a>
                </div>
            </div>
          </div>
          <div class="col m6 s12">
          <h5><i class="small material-icons" style="color: #E84627;">my_location</i>Adresse de livraison</h5>
          <div class="col s12 input-field">
            <i class="material-icons prefix" style="color: #E84627 !important;">pin_drop</i>
            <input type="text" id="Rue" name="Rue" placeholder='Numéro de rue'/>
          </div>
          <div class="col s12 input-field">
            <i class="material-icons prefix" style="color: #E84627 !important;">track_changes</i>
            <input type="text" id="Adresse"  name="Adresse" placeholder='Adresse'/>
          </div>
          <div class="col s12 input-field">
            <i class="material-icons prefix" style="color: #E84627 !important;">near_me</i>
            <input type="text" id="CP" name="CP" placeholder='Code Postal'/>
          </div>
          </div>
        </div>
      <!-- <div class="row">
            <div class="col m5">
              {!! Form::submit('Modifier', ['class' => 'btn-large button']) !!}
            </div>
        </div> -->
    </div>
      <div class="modal-footer" style="text-align:center;">
        <a href="#" class="modal-action btn-flat " style="float:none !important;margin-left: 120px;">Etape 1/<b>3</b></a>
        <a href="#" class="modal-action btnSuivant btn-flat ">Suivant</a>
      </div>
    </div>
    <!--  Modal ETAPE 2 -->
    <div id="modal2" class="modal modal-fixed-footer">
        <div class="modal-content">
          <div class="row">
              <div class="col s12 titre">
                <h4 style="margin-top:10px;">PAIEMENT</h4>
              </div>
          </div>
          <p style="text-align:center; font-size:1.24em;">Paiement du billet sélectionné pour un montant de <b>250€</b> :</p>
          <div class="row">
              <div class="col m8 offset-m2" style="text-align:center">
                  <div class="col m4">
                    <div class="cadre" id="visa" name="visa"><img src="../../img/visa.jpg" alt="visa" title="visa" id="visa" class="imagePaiement" height="50px"/></div>
                  </div>
                  <div class="col m4">
                    <div class="cadre" id="mastercard" name="mastercard"><img src="../../img/mastercard.jpeg" alt="mastercard" title="mastercard" id="mastercard" class="imagePaiement" height="50px"/></div>
                  </div>
                  <div class="col m4">
                    <div class="cadre" style="padding: 25px 0px" id="paypal" name="paypal"><img src="../../img/paypal.jpg" alt="paypal" title="paypal" id="paypal" class="imagePaiement" height="25px" /></div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col s12">
                <div class="col s12 m8 offset-m2">
                  <input type="text" id="NomTitulaire" name="NomTitulaire" placeholder='Nom du tilulaire*'/>
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col m12">
              <div class="col s12 m8 offset-m2">
                <input type="text" id="NumCarte" name="NumCarte" placeholder='Numéro de carte*'/>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col m12">
              <div class="col s12 m4 offset-m2">
                <input type="text" id="DateExp" name="DateExp" placeholder="Date d'expiration*"/>
              </div>
              <div class="col s12 m4">
                <input type="text" id="Cyptogramme" name="Cyptogramme" placeholder='Cyptogramme*'/>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12">
              <div class="col m8 offset-m2">
                <input type="checkbox" id="Condition" name="Condition"/><label for="Condition">J'accepte les conditions générales de vente de BMS</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer " style="text-align:center;">
          <a href="#" class="modal-action modal-close waves-effect btnPrecedent btn-flat left">Precédent</a>
          <a href="#" class="modal-action btn-flat " style="float:none !important;">Etape 2/<b>3</b></a>
          <a href="#" class="modal-action modal-close waves-effect btnSuivant2 btn-flat ">Suivant</a>
        </div>
      </div>
  <!--  Modal ETAPE 3 -->
      <div id="modal3" class="modal modal-fixed-footer">
          <div class="modal-content">
            <div class="row">
                <div class="col s12 titre">
                  <h4 style="margin-top:10px;">RECAPITULATIF</h4>
                </div>
            </div>
            <div class="row">
              <div class="col m12">
                <p style="text-align:center; font-size:1.24em;">Vous avez souscris à une offre <b>premium</b> pour un montant de <b>250€</b></p>
                <p>
                  Lorem ipsum dolor sit amet, ut usu petentium laboramus signiferumque, sed ullum populo ne, autem numquam postulant ut vis. Ad odio iuvaret molestie mel.<br><br> Detraxit nominati ex vim, alii choro antiopam mea in, laudem pertinacia cotidieque vix ut. Ius ei persecuti concludaturque, nobis tamquam luptatum in nam, ei porro everti disputationi vix. In pri quodsi evertitur, ex est praesent delicatissimi. Quas antiopam splendide usu no.

                  Ad eum illum posidonium. Sea cu zril postulant. Erat albucius voluptua eu per. Vel partem utroque democritum an, sed an error detracto quaestio.<br><br>

                  Timeam dolores pri no, tibique nominavi ne duo. Timeam postulant ex vis. Consul deseruisse an pri, dicta mollis perfecto eam no. Vix solum argumentum an, sea esse scaevola interesset in. Vix te tollit pertinax, at dolores comprehensam pro.

                  Ne duo illud scaevola, inermis fierent constituto at per. Sea ex mandamus sententiae, ad quo vituperata adversarium definitiones. Velit incorrupte an vim.<br><br>@ Tous les droits sont réservés.
                </p>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" class="modal-action modal-close waves-effect btnTerminer btn-flat " value="Terminer"/>
          </div>
      </div>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        var TypeAbonnement ="";
        var TypePaiement="";
        $('.wrapper').hide();
        $('.btnOffre').click(function() {
            $('.wrapper').hide();
            $('.blocAbonnement').css("background-color", "white");
            $(this).closest('.blocAbonnement').find('.wrapper').show();
            $(this).closest('.blocAbonnement').css("background-color", "#dddddd ");
            TypeAbonnement = $(this).parent().siblings('.Type').html();
        });
        $('.btnFree').click(function() {
            TypeAbonnement = $(this).parent().siblings('.Type').html();
        });
        $('.btnValider').click(function() {
          if (TypeAbonnement==''){
              swal("Veuillez sélectionner une offre", "", "error");
          }else{
            if (TypeAbonnement=='PREMIUM'){
              swal({
                title: "Êtes vous sur ?",
                text: "Vous souscrivez pour l'abonnement Premium",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#E84627",
                confirmButtonText: "Oui, je veux!",
                cancelButtonText: "Non, je ne veux pas!",
                closeOnConfirm: false,
                closeOnCancel: false
              },
              function(isConfirm){
                if (isConfirm) {
                   swal.close();
                   $('#modal1').openModal();
                    $('#abonnement').val(1);

                } else {
                  swal.close();
                }
              });
            }else{
             // swal("Félicitation!", "Vous avez sélectionné l'abonnement " + TypeAbonnement, "success");
                $('#abonnement').val(0);

            }
          }
        });
        $('.imagePaiement').click(function() {
          $('.imagePaiement').css('opacity', '0.1');
          $(this).css('opacity', '1');
          $('.cadre').css('border', '0px solid white')
          $(this).parent().css('border', '1px solid #E84627 ')
          TypePaiement = $(this).parent().attr("id");
        });
        $("#btnTerminer").click(function() {
            $('#SubmitForm').submit();
        });
        /*  $("#SubmitForm").submit(function(){
          var nom = $('#Nom').val();
          var prenom = $('#Prenom').val();
          var email= $('#Email').val();
          var tel = $('#Tel').val();
          var ville = $('#Ville').val();
          var entreprise = $('#Entreprise').val();
          var metier = $('#Metier').val();
          var rue = $('#Rue').val();
          var adresse = $('#Adresse').val();
          var cp = $('#CP').val();
          var choixPaiement = TypePaiement;
          var nomTitulaire = $('#NomTitulaire').val();
          var numCarte = $('#NumCarte').val();
          var dateExpiration = $('#DateExp').val();
          var Cryptogramme = $('#Cyptogramme').val();
          alert(choixPaiement);
        });*/
        $('.btnSuivant').click(function() {
          $('#modal1').closeModal();
          $('#modal2').openModal();
        });
        $('.btnPrecedent').click(function() {
          $('#modal2').closeModal();
          $('#modal1').openModal();
        });
        $('.btnSuivant2').click(function() {
          $('#modal2').closeModal();
          $('#modal3').openModal();
        });
    });
</script>
