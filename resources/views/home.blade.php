@extends('layouts.site-layout')

@section('title', 'Home')

@section('content')

    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <br><br>
            <h1 class="header center orange-text">Business Manager System</h1>
            <div class="row center">
                <h5 class="header col s12 light">Une application hors du commun permettant le pilotage global des besoins<br/> des petites entreprises en terme de gestion de données</h5>
            <br><br>
                <a class="btn waves-effect waves-light orange" onclick="$('#sequence-modal').openModal()">Découvrir la démo</a>
                <a href="/diapo" class="btn waves-effect waves-light" style="background-color: rgb(255,87,34);">Lancer la présentation</a>
            </div>

            {{--Start Sequence--}}
            <div id="sequence-modal" class="modal">
                <div class="modal-content">
                    <div id="sequence" class="seq">

                <div class="seq-screen">
                    <ul class="seq-canvas">

                        <li class="seq-step1 seq-in" id="step1">

                            <div class="seq-code-pane seq-valign">

                                <button type="button" class="seq-view-code">
                                    <i class="fa fa-angle-double-up"></i> Show Code
                                </button>

                                <div class="seq-code">

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">1</i>
                                        <p>Voici le dashboard de votre application.<br> Cliquez sur le + pour ajouter un tableau</p>
                                    </div>
                                </div>

                            </div>

                            <div class="seq-content seq-valign" data-seq>

                                <div class="seq-instruction">

                                    <img src="{{ URL::asset('img/presentation/dashboard.png') }}" alt="dashboard" style="height: 50%;" title="BMS - dashboard">

                                    {{--<div class="seq-title">--}}
                                        {{--<object class="seq-instruction-icon" data="images/bracket-open.svg" type="image/svg+xml">--}}
                                            {{----}}
                                        {{--</object>--}}

                                        {{--<h2>Step 1</h2>--}}

                                        {{--<object class="seq-instruction-icon" data="images/bracket-close.svg" type="image/svg+xml">--}}
                                            {{--<img src="images/bracket-close.png" alt="A closing code bracket" />--}}
                                        {{--</object>--}}
                                    {{--</div>--}}

                                    {{--<h3>Easily add CSS transitions to step-based applications using "in" and "out" CSS classes</h3>--}}

                                    {{--<div class="seq-step">--}}
                                        {{--<p>Step 1 is currently in its <code>in<sup class="seq-step-icon">1</sup></code> position. Watch what happens when you <a href="#" title="" class="seq-next">go to the next step →</a></p>--}}
                                    {{--</div>--}}
                                </div>

                            </div>
                        </li>

                        <li class="seq-step2" id="step2">
                            <div class="seq-code-pane seq-valign">

                                <button type="button" class="seq-view-code">
                                    <i class="fa fa-angle-double-up"></i> Show Code
                                </button>

                                <div class="seq-code">

                                    {{--<div>Voici la liste des tableaux de votre application, vous pouvez :</div><br><br>--}}

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">2</i>
                                        <div>Ajouter d'autres tableaux</div>
                                    </div>

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">3</i>
                                        <div>Ajouter des colonnes</div>
                                    </div>

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">4</i>
                                        <div>
                                            Supprimer les colonnes et tableaux
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="seq-content seq-valign" data-seq>

                                <div class="seq-instruction">

                                    <img src="{{ URL::asset('img/presentation/ajouter_tableau.png') }}" alt="dashboard" style="height: 50%;" title="BMS - dashboard">
                                    <br><br><br>
                                    <img src="{{ URL::asset('img/presentation/liste_tableau.png') }}" alt="dashboard" style="height: 50%;" title="BMS - dashboard">
                                </div>

                            </div>
                        </li>

                        <li class="seq-step3" id="step3">
                            <div class="seq-code-pane seq-valign">

                                <button type="button" class="seq-view-code">
                                    <i class="fa fa-angle-double-up"></i> Show Code
                                </button>

                                <div class="seq-code">

                                    {{--<div>Modifiez aisément vos tableaux et colonnes :</div><br><br>--}}

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">5</i>
                                        <div>Renommer vos tableaux</div>
                                    </div>

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">6</i>
                                        <div>Choisir le type de colonne</div>
                                    </div>

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">7</i>
                                        <div>
                                            Rendre la colonne obligatoire ou non
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="seq-content seq-valign" data-seq>

                                <div class="seq-instruction">

                                    <img src="{{ URL::asset('img/presentation/ajouter_tableau.png') }}" alt="dashboard" style="height: 50%;" title="BMS - dashboard">
                                    <br><br><br>
                                    <img src="{{ URL::asset('img/presentation/modifier_tableau.png') }}" alt="dashboard" style="height: 50%;" title="BMS - dashboard">

                                </div>

                            </div>
                        </li>

                        <li class="seq-step4" id="step4">
                            <div class="seq-code-pane seq-valign">

                                <button type="button" class="seq-view-code">
                                    <i class="fa fa-angle-double-up"></i> Show Code
                                </button>

                                <div class="seq-code">

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">8</i>
                                        <div>
                                            Ajouter une ou plusieurs entrées dans la table...
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="seq-content seq-valign" data-seq>
                                <div class="seq-code">

                                    {{--<p class="seq-touch-title seq-touch-title-1">Ajouter une ou plusieurs entrées dans la table...</p>--}}

                                    {{--<div class="seq-touch-title seq-touch-title-2">--}}
                                        {{--<p>Fully responsive with touch support</p>--}}
                                    {{--</div>--}}

                                    {{--<img class="seq-phone" srcset="{{ URL::asset('img/presentation/gestion_table.png') }}" alt="" />--}}

                                    {{--<img class="seq-swipe" src="{{ URL::asset('img/presentation/gestion_table.png') }}" alt="" />--}}
                                    <img src="{{ URL::asset('img/presentation/gestion_table.png') }}" alt="dashboard" style="height: 50%;" title="BMS - dashboard">
                                </div>
                            </div>
                        </li>

                        <li class="seq-step5" id="step5">
                            <div class="seq-code-pane seq-valign">

                                <button type="button" class="seq-view-code">
                                    <i class="fa fa-angle-double-up"></i> Show Code
                                </button>

                                <div class="seq-code">

                                    <div class="seq-code-block" data-seq>
                                        <i class="seq-step-icon">9</i>
                                        <div>
                                            Exportez vos tableaux en PDF, CSV ou XLS
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="seq-content seq-valign" data-seq>

                                <div class="seq-instruction">

                                    {{--<h3>All of the features you need for animated applications</h3>--}}

                                    <div class="seq-step">
                                        <img src="{{ URL::asset('img/presentation/export_table.png') }}" alt="dashboard" style="height: 50%;" title="BMS - dashboard">
                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>

                <fieldset class="seq-nav" aria-controls="sequence" aria-label="Slider buttons">
                    <button type="button" class="seq-prev" aria-label="Previous"><i class="fa fa-chevron-circle-left"></i> Previous</button>
                    <button type="button" class="seq-next" aria-label="Next">Next <i class="fa fa-chevron-circle-right"></i></button>
                </fieldset>
            </div>
                    <ul role="navigation" aria-label="Pagination" aria-controls="sequence" class="seq-pagination">
                        <li class="seq-current"><a href="#step1" rel="step1" title="Go to slide 1: Intro"><i class="fa fa-dashboard"></i> <span>Le dashboard</span></a></li>
                        <li><a href="#step2" rel="step2" title="Go to slide 2: How It Works"><i class="fa fa-th-list"></i> <span>Liste des tableaux <br> les colonnes</span></a></li>
                        <li><a href="#step3" rel="step3" title="Go to slide 3: Animation"><i class="fa fa-th-list"></i> <span>Liste des tableaux <br> Modifier une colonne</span></a></li>
                        <li><a href="#step4" rel="step4" title="Go to slide 4: Responsive/Touch"><i class="fa fa-plus"></i> <span>Ajouter une entrée <br> dans la table</span></a></li>
                        <li><a href="#step5" rel="step5" title="Go to slide 5: Features"><i class="fa fa-file-excel-o"></i> <span>Exportez vos tableaux</span></a></li>
                    </ul>
                </div>
            </div>
            {{--End Sequence--}}
        </div>
    </div>
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12 m12">
                    <div class="icon-block">
                        <p class="light light-align">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 img-responsive">
                    {!! Html::image('img/landing-database.jpg', 'BMS - Landing Database') !!}
                </div>
            </div>
            <!--   Icon Section   -->
            <div class="row">

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
                        <h5 class="center">Performante</h5>

                        <p class="light light-align">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
                        <h5 class="center">User Friendly</h5>

                        <p class="light light-align">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center light-blue-text"><i class="material-icons">settings</i></h2>
                        <h5 class="center">Facilement Configurable</h5>

                        <p class="light light-align">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <h5 class="center">L'Equipe BMS</h5>
        <div class="row slider-landing">
            <div class="col s10 m12">
                <div id="owl-landing" class="owl-carousel">
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team1.jpg') }}" alt="" title="BMS - team"> </div>
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team2.jpg') }}" alt="" title="BMS - team"> </div>
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team3.jpg') }}" alt="" title="BMS - team"> </div>
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team6.jpg') }}" alt="" title="BMS - team"> </div>
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team7.jpg') }}" alt="" title="BMS - team"> </div>
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team8.jpg') }}" alt="" title="BMS - team"> </div>
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team10.jpg') }}" alt="" title="BMS - team"> </div>
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team13.jpg') }}" alt="" title="BMS - team"> </div>
                    <div class="img-responsive-faces"><img src="{{ URL::asset('img/dev/team/team14.jpg') }}" alt="" title="BMS - team"> </div>
                </div>
            </div>
        </div>
    </div>

@stop
