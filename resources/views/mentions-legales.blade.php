@extends('layouts.site-layout')

@section('title', 'Mentions Légales')

@section('content')


    <div class="container presentation" style="padding-top:20px;">
    	<div class="row" id="mentions">
    		{!! $mentions->content !!}
    	</div>
	   
	</div>

@stop