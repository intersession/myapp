@extends('layouts.site-layout')

@section('title', 'Présentation')

@section('content')
	<div class="row">
		<div class="col s12 m12">
			<div id="owl-presentation" class="owl-carousel owl-theme">
				<div class="item"><img src="{{ URL::asset('img/presentation1.jpg') }}" alt="image1" title="image1"></div>
				<div class="item"><img src="{{ URL::asset('img/presentation2.jpg') }}" alt="image2" title="image2"></div>
				<div class="item"><img src="{{ URL::asset('img/presentation3.jpg') }}" alt="image3" title="image3"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m12">
			<h1 style="text-align: center"></h1>
			<div class="section-title">
				<h2>Présentation de l'entreprise BMS<span class="dark">BMS</span></h2>
			</div> <!-- .section-title ends -->
		</div>
	</div>
	<div class="container presentation">
		<div class="row">
			<div class="col s12 m12">
				<p>{!! $presentationGenerale->content !!}</p>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="container">
	<div class="clear"></div>
	<section class="service section-block">
		<div class="section-title">
			<h2>Nos Avantages<span class="dark">BMS</span></h2>
		</div> <!-- .section-title ends -->

		<div class="container">
			<div class="row section-content">
				<div class="col m4 col s6">
					<div class="icon-block"><i class="fa fa-database"></i></div>
					<h3>Base de données Puissante</h3>

						{!! $service1->content !!}

				</div> <!-- .col-md-4 col-sm-6 ends -->

				<div class="col m4 col s6">
					<div class="icon-block"><i class="fa fa-cogs"></i></div>
					<h3>Constructeur d'application intuitif</h3>

						{!! $service2->content !!}

				</div> <!-- .col-md-4 col-sm-6 ends -->

				<div class="col m4 col s6">
					<div class="icon-block"><i class="fa fa-shield"></i></div>
					<h3>Sécurisé, fiable et évolutif</h3>

						{!! $service2->content !!}

				</div> <!-- .col-md-4 col-sm-6 ends -->

				<div class="col m4 col s6">
					<div class="icon-block"><i class="fa fa-fighter-jet"></i></div>
					<h3>Performance de déploiement</h3>
					<p>
						Mnaoare kese hai tu bata more manoorate tomader kase omon bosease dekho na tomader kase jabo to chole kivabe jete hbe bolona bondhu.
					</p>
				</div> <!-- .col-md-4 col-sm-6 ends -->

				<div class="col m4 col s6">
					<div class="icon-block"><i class="fa fa-cloud"></i></div>
					<h3>Hebergé sur un cloud sécurisé</h3>
					<p>
						Takale tumi ami hotath onno deshe chole ase mon hariye bolse asi kivabe to janina make niye tomar majhe kiemn chle je tumi ta bolona ar koto kivabe.
					</p>
				</div> <!-- .col-md-4 col-sm-6 ends -->

				<div class="col m4 col s6">
					<div class="icon-block"><i class="fa fa-shopping-cart"></i></div>
					<h3>Coût modulable pour tous les budgets</h3>
					<p>
						Chupe tomake dekhte jabo sei bikele tomar pashe bose asi ami tumi nei keno amar pashe. Kothay je chile geso evabe amake eka fele ore manoare.
					</p>
				</div> <!-- .col-md-4 col-sm-6 ends -->
			</div> <!-- .row section-content ends -->
		</div><!-- .container ends -->
	</section>

	<div class="row">
		<div class="col s12 m12">
			<img src="{{ URL::asset('img/presentation-footer.jpg') }}" width="100%">
		</div>
	</div>
	<div class="row presentation">
		<div class="col s12 m12">
			<p>{!! $accroche->content !!}</p>
		</div>
	</div>

</div>
@stop
