<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <meta charset="UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <title>{{ $page_title or "AdminLTE Dashboard" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ URL::asset("admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset("admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css") }}"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/skins/_all-skins.min.css") }}"/>

</head>
<body class="skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('header')

        <!-- Sidebar -->
        @include('sidebar')
        <div class="container">
            <div class="content">
                <div class="box">
                    <div class="box-header">
                        <h3>Ajouter un  utilisateur</h3>
                    </div>
                    <div class="box-body">

                        <!-- if there are creation errors, they will show here -->
                        {!! Html::ul($errors->all()) !!}

                        {!! Form::open(array('url' => 'admin/user/store')) !!}


                        <div class="box">
                            <div class="box-header">
                                <h2>Informations du profil</h2>
                            </div>

                            <div class="box-body">
                                {!! Form::label('lastname', 'Lastname') !!}
                                {!! Form::text('lastname', Input::old('lastname'), array('class' => 'form-control')) !!}


                                {!! Form::label('firstname', 'firstname') !!}
                                {!! Form::text('firstname', Input::old('firstname'), array('class' => 'form-control')) !!}

                                {!! Form::label('email', 'Email') !!}
                                {!! Form::email('email', Input::old('email'), array('class' => 'form-control')) !!}<br/>

                                {!! Form::label('password', 'Mot de passe') !!}<br/>
                                {!! Form::password('password', '', array('class' => 'form-control')) !!}<br/><br/>

                                {!! Form::label('password_validate', 'Valider mot de passe') !!}<br/>
                                {!! Form::password('password_validate', '', array('class' => 'form-control')) !!}<br/><br/>


                                {!! Form::label('birthday', 'Date de Naissance') !!}
                                {!! Form::text('birthday', Input::old('birthday'), array('class' => 'form-control')) !!}

                                {!! Form::label('address', 'Adresse') !!}
                                {!! Form::text('address', Input::old('address'), array('class' => 'form-control')) !!}

                                {!! Form::label('postal_code', 'CP') !!}
                                {!! Form::text('postal_code', Input::old('postal_code'), array('class' => 'form-control')) !!}

                                {!! Form::label('city', 'City') !!}
                                {!! Form::text('city', Input::old('city'), array('class' => 'form-control')) !!}

                                {!! Form::label('permissions', 'Permissions') !!}
                                {!! Form::text('permissions', Input::old('permissions'), array('class' => 'form-control')) !!}

                            </div>

                            <div class="box-header">
                                <h2>Informations sur la compagnie</h2>
                            </div>

                            <div class="box-body">
                                {!! Form::label('company_name', 'Nom de la compagnie') !!}
                                {!! Form::text('company_name', Input::old('company_name'), array('class' => 'form-control')) !!}

                                {!! Form::label('company_address', 'Adresse') !!}
                                {!! Form::text('company_address', Input::old('company_address'), array('class' => 'form-control')) !!}

                                {!! Form::label('company_postal_code', 'CP') !!}
                                {!! Form::text('company_postal_code', Input::old('company_postal_code'), array('class' => 'form-control')) !!}

                                {!! Form::label('company_city', 'Ville') !!}
                                {!! Form::text('company_city', Input::old('company_city'), array('class' => 'form-control')) !!}

                            </div>
                        </div>

                        {!! Form::submit('Create the User!', array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}

                    </div>
                </div>               
            </div>
        </div>
    <!-- Footer -->
    @include('footer')
    </div>
</body>
</html>
