<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <meta charset="UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <title>{{ $page_title or "AdminLTE Dashboard" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ URL::asset("admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset("admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css") }}"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/skins/_all-skins.min.css") }}"/>

</head>
<body class="skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('header')

        <!-- Sidebar -->
        @include('sidebar')
        <div class="container">

            <div class="content">
                <div class="box">
                    <div class="box-header">
                        <h3>Modification du profil</h3>
                    </div>
                    <div class="box-body">

                        <!-- if there are creation errors, they will show here -->
                        {!! Html::ul($errors->all()) !!}

                        {!! Form::model($user, array('route' => array('profile_update', $user->id), 'method' => 'POST')) !!}


                        <div class="box">
                            <div class="box-header">
                                <h2>Informations du profil</h2>
                            </div>

                            <div class="box-body">
                                {!! Form::label('lastname', 'lastname') !!}
                                {!! Form::text('lastname', $user->lastname , array('class' => 'form-control')) !!}

                                {!! Form::label('firstname', 'firstname') !!}
                                {!! Form::text('firstname', $user->firstname , array('class' => 'form-control')) !!}

                                {!! Form::label('email', 'Email') !!}
                                {!! Form::email('email', $user->email , array('class' => 'form-control')) !!}

                                {!! Form::label('birthday', 'Date de Naissance') !!}
                                {!! Form::text('birthday', $user->information->birthday , array('class' => 'form-control')) !!}

                                {!! Form::label('address', 'Adresse') !!}
                                {!! Form::text('address', $user->information->address , array('class' => 'form-control')) !!}

                                {!! Form::label('postal_code', 'CP') !!}
                                {!! Form::text('postal_code', $user->information->postal_code , array('class' => 'form-control')) !!}

                                {!! Form::label('city', 'City') !!}
                                {!! Form::text('city', $user->information->city , array('class' => 'form-control')) !!}

                                {!! Form::label('permissions', 'Permissions') !!}
                                {!! Form::text('permissions', $user->permissions , array('class' => 'form-control')) !!}

                            </div>

                            <div class="box-header">
                                <h2>Informations sur la compagnie : <b> {{ $user->company->name }} </b></h2>
                                <i>Le nom de la compgnie n'est pas modifiable car il est lié à la base de données du client</i>
                            </div>

                            <div class="box-body">
                                {!! Form::label('company_address', 'Adresse') !!}
                                {!! Form::text('company_address', $user->company->address , array('class' => 'form-control')) !!}

                                {!! Form::label('company_postal_code', 'CP') !!}
                                {!! Form::text('company_postal_code', $user->company->postal_code , array('class' => 'form-control')) !!}

                                {!! Form::label('company_city', 'Ville') !!}
                                {!! Form::text('company_city', $user->company->city , array('class' => 'form-control')) !!}

                            </div>
                        </div>

                        {!! Form::submit('Edit my profile!', array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}

                    </div>
                </div>               
            </div>
        </div>
    <!-- Footer -->
    @include('footer')
    </div>
</body>
</html>
