<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <meta charset="UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <title>{{ $page_title or "AdminLTE Dashboard" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ URL::asset("admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset("admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css") }}"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/skins/_all-skins.min.css") }}"/>

</head>
<body class="skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('header')

        <!-- Sidebar -->
        @include('sidebar')
        <div class="container">

            <div class="content">
                <div class="box">
                    <div class="box-header">
                        <h3>Modification de l'utilisateur {{ $user->lastname }} {{ $user->firstname }}</h3>
                    </div>
                    <div class="box-body">

                        <!-- if there are creation errors, they will show here -->
                        {!! Html::ul($errors->all()) !!}

                        {!! Form::model($user, array('route' => array('abonnement_update', $user->id), 'method' => 'POST')) !!}


                        <div class="box">
                            <div class="box-header">
                                <h2>Informations concernant l'abonnement</h2>
                            </div>

                            <div class="box-body">


                                {!! Form::label('level', 'Level') !!}<br/>


                                <?php if($user->abonnement->level == 1) { ?>
                                    <p>Gratuit : 
                                    {!! Form::radio('level', '0') !!}</p>
                                    <p>Premium : 
                                    {!! Form::radio('level', '1', true) !!}</p><br/>

                                <?php } else { ?>

                                    <p>Gratuit : 
                                    {!! Form::radio('level', '0',true) !!}</p>
                                    <p>Premium : 
                                    {!! Form::radio('level', '1') !!}</p><br/>

                                <?php } ?>
                                <br/>                                
                                

                                {!! Form::label('starting_date', 'Date de début') !!}
                                {!! Form::text('starting_date', $user->abonnement->starting_date , array('class' => 'form-control')) !!}

                                {!! Form::label('ending_date', 'Date de fin') !!}
                                {!! Form::text('ending_date', $user->abonnement->ending_date , array('class' => 'form-control')) !!}

                            </div>
                        </div>

                        {!! Form::submit('Edit the Subscription!', array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}

                    </div>
                </div>               
            </div>
        </div>
    <!-- Footer -->
    @include('footer')
    </div>
</body>
</html>
