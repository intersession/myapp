<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <meta charset="UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <title>{{ $page_title or "AdminLTE Dashboard" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ URL::asset("admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset("admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css") }}"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/skins/_all-skins.min.css") }}"/>

</head>
<body class="skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('header')

        <!-- Sidebar -->
        @include('sidebar')
        <div class="container">
            <div class="content">
                <div class="box">
                    <div class="box-header">
                        <h3>Détail de l'utilisateur : {{ $user->firstname }} {{ $user->lastname }}</h3>
                    </div>
                    <div class="box-body">                      
                        <div class="box">
                        <div class="box-header">
                            <h2>Profil</h2>
                        </div>
                        <div class="box-body">
                            <p>Nom : {{ $user->lastname }}</p>
                            <p>Prénom : {{ $user->firstname }}</p>
                            <p>Email : {{ $user->email }}</p>
                            <p>Adresse : {{ $user->information->address }}</p>
                            <p>CP : {{ $user->information->postal_code }}</p>
                            <p>Ville : {{ $user->information->city }}</p>
                            <p>Date de naissance : {{ $user->information->birthday }}</p>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header">
                            <h2>Compagnie</h2>
                        </div>
                        <div class="box-body">
                            <p>Compagnie : {{ $user->company->name }}</p>
                            <p>Adresse : {{ $user->company->address }}</p>
                            <p>CP : {{ $user->company->postal_code }}</p>
                            <p>Ville : {{ $user->company->city }}</p>
                        </div>
                    </div>
                    </div>                    
                </div>
            </div>
        </div>
    <!-- Footer -->
    @include('footer')
    </div>
</body>
</html>
