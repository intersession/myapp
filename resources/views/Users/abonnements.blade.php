<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <meta charset="UTF-8">

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <title>{{ $page_title or "BMS Dashboard" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ URL::asset("admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset("admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/AdminLTE.min.css") }}"/>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL::asset("admin-lte/dist/css/skins/_all-skins.min.css") }}"/>

</head>
<body class="skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('header')

        <!-- Sidebar -->
        @include('sidebar')
        <div class="container">
            <div class="content">
                <div class="box">
                    <div class="box-header">
                        <h3>Gestion des abonnements</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Email</th>
                                <th>Compagnie</th>
                                <th>Type</th>
                                <th>Actions</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($users as $user)
                                <tr>
                                    @if($user->company)

                                    <td>{{ $user->lastname }}</td>
                                    <td>{{ $user->firstname }}</td>
                                    <td>{{ $user->email }} </td>
                                    <td>{{ $user->company->name }}</td>
                                        <td>
                                            <?php if($user->level == 1) { ?>
                                            Gratuit :
                                            <?php } else { ?>
                                            Premium :
                                            <?php } ?>

                                        </td>
                                    <td>
                                        <a href="{{ URL::to('admin/abonnement/' . $user->id) }}"><i class="ion ion-edit"></i></a>
                                    </td>
                                    @endif
                                    

                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>                
            </div>
        </div>
    <!-- Footer -->
    @include('footer')
    </div>
</body>
</html>
