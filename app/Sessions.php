<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model {

	protected $table = 'sessions';
	public $timestamps = false;

	public function hasSessions()
	{
		return $this->hasMany('App\Users');
	}

}