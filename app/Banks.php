<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banks extends Model {

	protected $table = 'banks';
	public $timestamps = false;

}