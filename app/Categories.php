<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model {

	protected $table = 'categories';
	public $timestamps = false;

	public function __construct()
	{
		parent::setConnection('mysql_api');
	}

	public function hasEntities()
	{
		return $this->hasMany('App\Entities');
	}

}