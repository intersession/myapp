<?php

namespace App\Repositories;

use App\Pages;
Use DB;

class PagesRepository
{

    protected $page;

	public function __construct(Pages $pages)
	{
		$this->page = $pages;
	}


	/**
	* MÃ©thode pour modifier une page existante en base de donnÃ©e
	*
	* @param $pages = le Model Pages
	*
	* @param $inputs = valeur des champs enregistrÃ©s
	*/
	private function save(Pages $pages, Array $inputs)
	{
	  	$pages->content = $inputs['content'];
		$pages->save();
	}

	/**
	* Méthode pour générer la pagination
	*
	* @param $n = nombre de pages à afficher par page
	*/
	public function getPaginate($n)
	{
		return $this->page->paginate($n);
	}

	/**
	* MÃ©thode pour retrouver une page par son id
	*
	* @param $id = id de la page
	*/
	public function getById($id)
	{
	    return $this->page->findOrFail($id);
	}

	/**
	* MÃ©thode pour modifier une page
	*
	* @param $id = id de la page
	*
	* @param $inputs = valeur des champs existants
	*/
	public function update($id, Array $inputs)
	{
		// enregistrement dans la table pages de la nouvelle valeur
		$this->save($this->getById($id), $inputs);
	}

}