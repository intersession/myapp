<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Columns extends Model {

	protected $table = 'columns';
	public $timestamps = false;

	const INTEGER = 'integer';
	const FLOAT = 'float';
	const STRING = 'string';
	const BOOLEAN = 'boolean';
	const DATE = 'date';

	public function __construct()
	{
		parent::setConnection('mysql_api');
	}

	public function hasCategory()
	{
		return $this->hasOne('App\Categories');
	}

}
