<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DateTime;
use DateInterval;
use App\Providers\GapiServiceProvider as gapi;

class StatsServiceProvider extends ServiceProvider {

    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /* Fonction retournant les pages vues accompagnées des visites et des vues liées à cette page sous forme d'un tableau de 3 cellules :

    Paramètres :
        - D : retourne les valeurs du jour
        - W : retourne les valeurs de la semaine
        - M : retourne les valeurs du mois
        - Y : retourne les valeurs de l'année
    Valeur de retour :
    - tableau contenant l'ensemble des pages (array('url','nb page vue pour la page','nb visites pour la page'))
    ---> Attention : retourne false si aucune page retournée
    */
    public static function getTop5PageVisit($periodicite)
    {
        $return = array();
        $gaEmail = 'handy-buttress-117412@appspot.gserviceaccount.com';
        $gaPassword = public_path().'/Project-f1e007b221e5.p12';
        $profileId = '104653403';

        $dimensions = array('pagePath');
        $metrics = array('pageviews','visits');
        $sortMetric = array('-visits','-pageviews');
        $filter = null;

        $startDate = new DateTime();
        $startDate = $startDate->sub(new DateInterval('P1'.$periodicite))->format('Y-m-d');
        $endDate = date("Y-m-d", time());
        $startIndex = 1;
        $maxResults = 5;

        $ga = new gapi($gaEmail, $gaPassword);
        $ga->requestReportData($profileId, $dimensions, $metrics, $sortMetric, $filter, $startDate, $endDate, $startIndex, $maxResults);

        $i = 0;
        foreach ($ga->getResults() as $result) {
            $return[$i]['url'] = $result->getPagePath();
            $return[$i]['visits'] = $result->getVisits();
            $return[$i]['views'] = $result->getPageviews();
            $i++;
        }

        if(count($return) == 0)
            $return = false;

        return $return;
    }

    public static function getStatsVisitsViewsTotal($periodicite)
    {
        $return = array();
        $gaEmail = 'handy-buttress-117412@appspot.gserviceaccount.com';
        $gaPassword = 'Project-f1e007b221e5.p12';
        $profileId = '104653403';

        if($periodicite == 'Y') {
            $dimensions = array('month', 'year');
            $sortMetric = array('year', 'month');
        }
        else {
            $dimensions = array('day','month','year');
            $sortMetric = array('year','month','day');
        }
        $metrics = array('pageviews','visits');

        $filter = null;

        $startDate = new DateTime();
        $startDate = $startDate->sub(new DateInterval('P1'.$periodicite))->format('Y-m-d');
        $endDate = date("Y-m-d", time());
        $startIndex = 1;
        $maxResults = 10000;

        $ga = new gapi($gaEmail, $gaPassword);
        $ga->requestReportData($profileId, $dimensions, $metrics, $sortMetric, $filter, $startDate, $endDate, $startIndex, $maxResults);

        $reponse = $ga->getResults();
        array_shift($reponse);

        $i = 0;
        foreach ($reponse as $result) {
            $return[$i]['visits'] = $result->getVisits();
            $return[$i]['views'] = $result->getPageViews();

            if($periodicite == 'Y')
                $return[$i]['date'] = $result->getMonth().'/'.$result->getYear();
            else
                $return[$i]['date'] = $result->getDay().'/'.$result->getMonth().'/'.$result->getYear();

            $i++;
        }

        if(count($return) == 0)
            $return = false;

        return $return;
    }

    /* Taux de rebond
       Paramètre :
        - D : retourne le taux de rebond du jour
        - W : retourne les taux de rebond des 7 derniers jours
        - M : retourne les taux de rebond du mois (29/30/31 jours glissants) par jour
        - Y : retourne les taux de rebond sur les 12 derniers mois
       Retour :
        - un tableau contenant deux tableaux : un tableau des abscisses en indice 0 et les valeurs en indice 1
        - false si aucun résultat
    */
    public static function getVisitBounceRate($periodicite)
    {
        $return = array();
        $gaEmail = 'handy-buttress-117412@appspot.gserviceaccount.com';
        $gaPassword = 'Project-f1e007b221e5.p12';
        $profileId = '104653403';

        if($periodicite == 'Y') {
            $dimensions = array('month', 'year');
            $sortMetric = array('year', 'month');
        }
        else {
            $dimensions = array('day','month','year');
            $sortMetric = array('year','month','day');
        }

        $metrics = array('visitBounceRate');

        $filter = null;

        $startDate = new DateTime();
        $startDate = $startDate->sub(new DateInterval('P1'.$periodicite))->format('Y-m-d');
        $endDate = date("Y-m-d", time());
        $startIndex = 1;
        $maxResults = 10000;

        $ga = new gapi($gaEmail, $gaPassword);
        $ga->requestReportData($profileId, $dimensions, $metrics, $sortMetric, $filter, $startDate, $endDate, $startIndex, $maxResults);

        if(count($ga->getResults()) > 0) {
            $reponse = $ga->getResults();
            array_shift($reponse);

            $i = 0;
            foreach ($reponse as $r) {
                if ($periodicite == 'Y')
                    $return[$i]['date'] = $r->getMonth() . '/' . $r->getYear();
                else
                    $return[$i]['date'] = $r->getDay() . '/' . $r->getMonth() . '/' . $r->getYear();

                $return[$i]['bounceRate'] = round($r->getVisitBounceRate(), 2);

                $i++;
            }
        }

        if(count($return) == 0)
            $return = false;

        return $return;
    }

    /* Temps Moyen de la session
       Paramètre :
        - D : retourne le temps moyen d'une session aujourd'hui
        - W : retourne les temps moyen d'une session des 7 derniers jours par jour
        - M : retourne les temps moyen d'une session du mois (29/30/31 jours glissants) par jour
        - Y : retourne les temps moyen d'une session sur les 12 derniers mois par mois
       Retour :
        - un tableau contenant deux tableaux : un tableau des abscisses en indice 0 et les valeurs en indice 1
        - false si aucun résultat
    */
    public static function getAvgTimeOnSite($periodicite)
    {
        $return = array();
        $gaEmail = 'handy-buttress-117412@appspot.gserviceaccount.com';
        $gaPassword = 'Project-f1e007b221e5.p12';
        $profileId = '104653403';

        if($periodicite == 'Y') {
            $dimensions = array('month', 'year');
            $sortMetric = array('year', 'month');
        }
        else {
            $dimensions = array('day','month','year');
            $sortMetric = array('year','month','day');
        }

        $metrics = array('avgtimeOnSite');
        $filter = null;
        $startDate = new DateTime();
        $startDate = $startDate->sub(new DateInterval('P1'.$periodicite))->format('Y-m-d');
        $endDate = date("Y-m-d", time());
        $startIndex = 1;
        $maxResults = 10000;

        $ga = new gapi($gaEmail, $gaPassword);
        $ga->requestReportData($profileId, $dimensions, $metrics, $sortMetric, $filter, $startDate, $endDate, $startIndex, $maxResults);

        if(count($ga->getResults()) > 0) {
            $reponse = $ga->getResults();
            array_shift($reponse);

            $i = 0;
            foreach ($reponse as $r) {
                if ($periodicite == 'Y')
                    $return[$i]['date'] = $r->getMonth() . '/' . $r->getYear();
                else
                    $return[$i]['date'] = $r->getDay() . '/' . $r->getMonth() . '/' . $r->getYear();

                $return[$i]['avg'] = gmdate("H:i:s", $r->getavgTimeOnSite());
                $i++;
            }
        }

        if(count($return) == 0)
            $return = false;

        return $return;
    }

    /* Fonction retournant les visites (sessions) par catégorie de Device (mobile, tablette, desktop) :
    Paramètres :
        - D : retourne les valeurs du jour
        - W : retourne les valeurs de la semaine
        - M : retourne les valeurs du mois
        - Y : retourne les valeurs de l'année
    Valeur de retour :
    - Tableau associatif de 3 valeurs : exemple : Array ( [mobile] => 13 [tablet] => 1 [desktop] => 27 )
    - Retourne false si aucune résultat
    */
    public static function getDeviceCategory($periodicite)
    {
        $return = array();
        $gaEmail = 'handy-buttress-117412@appspot.gserviceaccount.com';
        $gaPassword = 'Project-f1e007b221e5.p12';
        $profileId = '104653403';

        $dimensions = array('deviceCategory','day','month', 'year');
        $sortMetric = array('year', 'month','day');


        $metrics = 'visits';
        $filter = null;

        $startDate = new DateTime();
        $startDate = $startDate->sub(new DateInterval('P1'.$periodicite))->format('Y-m-d');
        $endDate = date("Y-m-d", time());
        $startIndex = 1;
        $maxResults = 10000;

        $ga = new gapi($gaEmail, $gaPassword);
        $ga->requestReportData($profileId, $dimensions, $metrics, $sortMetric, $filter, $startDate, $endDate, $startIndex, $maxResults);

        if(count($ga->getResults()) > 0 ) {
            $reponse = $ga->getResults();

            $compte = 0;

            for($i=0;$i<=2;$i++){
                if($startDate == $reponse[$i]->getYear().'-'.$reponse[$i]->getMonth().'-'.$reponse[$i]->getDay())
                    $compte++;
            }

            array_splice($reponse, 0, -(count($reponse) - $compte));

            $return['mobile'] = 0;
            $return['tablet'] = 0;
            $return['desktop'] = 0;
            
            foreach ($reponse as $r) {
                if($r->getDeviceCategory() == 'mobile')
                    $return['mobile'] += $r->getVisits();
                elseif($r->getDeviceCategory() == 'tablet')
                    $return['tablet'] += $r->getVisits();
                elseif($r->getDeviceCategory() == 'desktop')
                    $return['desktop'] += $r->getVisits();
            }

            return $return;
        }
        return false;
    }
}