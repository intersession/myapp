<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization, Content-Type' );
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers:  X-Requested-With, Content-Type, X-Auth-Token, Origin, Authorization');
header('Access-Control-Allow-Methods:GET,PUT,POST,DELETE');

/*
|--------------------------------------------------------------------------
| DEBUT PARTIE FRONT-OFFICE
|
*/
Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/abonnement', function () {
    return view('abonnement');
});

Route::get('/diapo', function () {
    return view('diapo');
})->name('diapo');
/*Route::get('/landing', function () {
    return view('landing');
})->name('landing');*/

/*Route::get('/profil', function () {
    return view('profil');
});*/

/*Route::get('/pre-register', function () {
    return view('pre-register');
});*/

/*Route::get('/register', function () {
    return view('register');
});*/

/*Route::get('/login', function () {
    return view('login');
});*/

Route::get('auth/login', 'Auth\AuthController@getLogin')
    ->name('auth_login');

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/register', 'RegisterController@index');
Route::post('/register', 'RegisterController@create');

Route::get('forgetpassword', 'Auth\AuthController@forgetPassword')->name('forget_password');

Route::post('/abonnement', 'RegisterController@registerAbonnement');
Route::get('/abonnement', 'RegisterController@abonnement');
Route::get('abonnement/{id}', 'RegisterController@abonnement');


Route::get('/confidentialite', function () {
    return view('confidentialite');
})->name('confidentialite');

Route::get('/presentation', 'PagesController@displayPresentation');
Route::get('/mentions-legales', 'PagesController@displayMentions')
    ->name('mentions-legales');
Route::get('/cgv', 'PagesController@displayCgv')
    ->name('cgv');

Route::resource('pages', 'PagesController', ['only' => ['index','edit','update']]);

/*
|--------------------------------------------------------------------------
| FIN PARTIE FRONT-OFFICE
|
*/

/*
|--------------------------------------------------------------------------
| DEBUT PARTIE BACK-OFFICE
|
*/


Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{
    Route::get('', 'StatsController@index')->name('admin_dashboard');

    Route::resource('pages', 'PagesController', ['only' => ['index','edit','update']]);

    Route::get('abonnement/{id}', 'UsersController@abonnement')
        ->where('id', '[0-9]+');

    Route::post('abonnement/{id}', 'UsersController@abonnement_update')
        ->name('abonnement_update')
        ->where('id', '[0-9]+');



    Route::get('abonnements/', 'UsersController@abonnements');


    Route::group(['prefix' => 'user'],   function()
    {
        Route::get('delete/{id}', 'UsersController@destroy')
            ->where('id', '[0-9]+');

        Route::post('update/{id}', 'UsersController@update')
            ->name('user_update')
            ->where('id', '[0-9]+');

        Route::post('update/{id}', 'UsersController@profile_update')
            ->name('profile_update')
            ->where('id', '[0-9]+');


        Route::get('edit/{id}', 'UsersController@edit')
            ->name('user_update')
            ->where('id', '[0-9]+');


        // Registration routes...
        Route::get('auth/register', 'Auth\AuthController@getRegister');
        Route::post('auth/register', 'Auth\AuthController@postRegister')
            ->name('register');

        Route::post('store', 'UsersController@store');
        Route::get('profile', 'UsersController@profile');

        Route::get('create', 'UsersController@create')
            ->name('user_create');

        Route::get('{id}', 'UsersController@show')
            ->name('user_show')
            ->where('id', '[0-9]+');

        Route::get('/', 'UsersController@index');
    });

    Route::get('stats/{periode}', 'StatsController@index')
        ->where('periode', '[DWMY]');
});



/*
|--------------------------------------------------------------------------
| FIN PARTIE BACK-OFFICE
|
*/

/*
|--------------------------------------------------------------------------
| DEBUT PARTIE API
|
*/
Route::group(['prefix' => 'api'],  function ()
{
  Route::group(['prefix' => 'v1'],   function()
  {
    Route::group(['prefix' => 'auth'],   function()
    {
      Route::post('login', 'Auth\AuthController@authentication')
        ->name('api_authenticate');
      Route::get('login', function() {
          return Redirect::to('http://ipssi-lyon.fr:8080');
      })->name('api_login_angular');
      Route::post('logout', 'Auth\AuthController@getLogout')
        ->name('api_getLogout');
    });

    //PARTIE USER
    Route::get('user/{id?}', 'Api\UsersApiController@show');
    Route::put('user/edit/{id}', 'Api\UsersApiController@update');
    Route::get('user/invoice/{id}', 'Api\UsersApiController@inVoice');

    Route::get('timebyperiod/{id}/{dateOne}/{dateTwo}', 'Api\UsersApiController@timeByPeriod');

    //PARTIE ABONNEMENT
    Route::get('abonnement/show/{id?}', 'Api\AbonnementsApiController@show');

    Route::post('table', 'Api\TablesApiController@create');
    Route::get('table', 'Api\TablesApiController@getAll');
    Route::get('table/{id}', 'Api\TablesApiController@get');
    Route::put('table/{id}', 'Api\TablesApiController@update');
    Route::delete('table/{id}', 'Api\TablesApiController@destroy');

    Route::post('row', 'Api\RowsApiController@create');
    Route::put('row/{id}', 'Api\RowsApiController@update');
    Route::delete('row/{idtable}/{id}', 'Api\RowsApiController@destroy');

    //Route::post('columns', 'Api\ColumnsApiController@update');
  });
});
/*
|--------------------------------------------------------------------------
| FIN PARTIE API
|
*/
