<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Users;
use App\Subscriptions;
use App\Companies;
use App\Http\Controllers\Api\UsersApiController;
use Session;

class DashboardController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    // we recover the connect user
    $currentUser = Users::find(1);
    $arrayReturn = array();

    // if the current user exist ...
    if($currentUser != null){
      // we recover the subscription
      $subscription = Subscriptions::all()->where('id_users', $currentUser->id)->first();

      // if the subscription exist and if the date is in the period of the subscription ...
      if($subscription != null && Subscriptions::hasCurrentSubscription($subscription)){
        // subscription
        // we recover name of the subscription
        $arrayReturn['subscription'] = Subscriptions::getNameLevel($subscription);
      }

      // numbers of users
      // we recover all users which are linked to the company
      $arrayReturn['numberUsersCompany'] = Users::all()->where('id_companies', $currentUser->id_companies)->count();

      // time
      $time = UsersApiController::timeByPeriod($currentUser->id, time(), time());
      $hour = floor($time / 3600);
      $decimalPart = $time / 3600 - floor($time / 3600);
      $minute = round($decimalPart * 3600);

      $arrayReturn['timeToday'] = $hour . 'h ' . $minute . 'm';
    }

    return view('admin', $arrayReturn);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }

}

?>
