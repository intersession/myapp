<?php namespace App\Http\Controllers;

use App\Users;
use App\UsersInformations;
use App\Companies;
use Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class UsersController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    //Script qui permet de créer une bdd à déplacer en temps voulu
    //DB::select(DB::raw('CREATE DATABASE dbname'));

    return view('Users/index', ['users' => Users::all()]);
  }

  public function profile()
  {
    return view('Users/profile', ['user' => Users::findOrFail(Session::get('idUser'))]);
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('Users/create');
  }

  public function abonnement($id_user)
  {
    return view('Users/abonnement', ['user' => Users::findOrFail(1)]);
  }

  public function abonnements()
  {
    return view('Users/abonnements', ['users' => Users::all()]);
  }

  public function abonnement_update($id)
  {

      if(Input::get('starting_date') >= Input::get('ending_date'))
      {
        return Redirect::to('admin/abonnement/' . $id )
            ->withErrors("La date de début est supérieure à la date de fin ! ");
      }

      // validate
      // read more on validation at http://laravel.com/docs/validation
      $rules = array(
          'level'    => 'required',
          'starting_date'   => 'required|date',
          'ending_date'     => 'required|date'
      );

      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
        return Redirect::to('admin/abonnement/' . $id )
            ->withErrors($validator);
      } else {

        $user = Users::find($id);
        $abonnement = $user->abonnement;
        $abonnement->level         = Input::get('level');
        $abonnement->starting_date = Input::get('starting_date');
        $abonnement->ending_date   = Input::get('ending_date');
        $abonnement->save();

        // redirect
        Session::flash('message', 'Successfully updated subscription!');
        return Redirect::to('admin/abonnements');
      }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {

    // validate
    // read more on validation at http://laravel.com/docs/validation
    $rules = array(
        'lastname'            => 'required',
        'firstname'           => 'required',
        'email'               => 'required|email|unique:users,name',
        'password'            => 'required|min:6|',
        'password_validate'   => 'required|min:6|same:password',
        'company'             => 'required',
        'birthday'            => 'required',
        'city'                => 'required',
        'postal_code'         => 'required',
        'address'             => 'required',
        'company_name'        => 'required|unique:companies,name',
        'company_address'     => 'required',
        'company_postal_code' => 'required',
        'company_city'        => 'required'
    );


    $validator = Validator::make(Input::all(), $rules);

    // process the login
    if ($validator->fails()) {
      return Redirect::to('admin/user/create')
          ->withErrors($validator)
          ->withInput(Input::except('password'));
    } else {


      //company
      $company = new Companies;
      $company->name = Input::get('company_name');
      $company->address = Input::get('company_address');
      $company->postal_code = Input::get('company_postal_code');
      $company->city = Input::get('company_city');
      $company->save();

      //user
      $user = new Users;
      $user->lastname     = Input::get('lastname');
      $user->firstname    = Input::get('firstname');
      $user->email        = Input::get('email');
      $user->password     = Input::get('password');
      $user->permissions  = Input::get('permissions');
      $user->save();


      //Informations
      $information = new UsersInformations;
      $information->birthday    = Input::get('birthday');
      $information->address     = Input::get('address');
      $information->city        = Input::get('city');
      $information->postal_code = Input::get('postal_code');
      $information->save();



      // redirect
      Session::flash('message', 'Successfully created user!');
      return Redirect::to('admin/user');
    }

  }

  public function profile_update($id)
  {

    // validate
    // read more on validation at http://laravel.com/docs/validation
    $rules = array(
        'lastname'    => 'required',
        'firstname'   => 'required',
        'email'       => 'required|email',
        'permissions' => 'max:200',
        'address'     => 'max:100',
        'city'        => 'max:50',
        'postal_code' => 'max:15',
        'birthday'    => 'date'
    );

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->fails()) {
      return Redirect::to('admin/user/edit/' . $id . '')
          ->withErrors($validator);
    } else {

      $user = Users::find($id);
      $user->lastname                 = Input::get('lastname');
      $user->firstname                = Input::get('firstname');
      $user->email                    = Input::get('email');
      $user->permissions              = Input::get('permissions');
      $user->save();

      $information = $user->information;
      $information->birthday         = Input::get('birthday');
      $information->address          = Input::get('address');
      $information->city             = Input::get('city');
      $information->postal_code      = Input::get('postal_code');
      $information->save();

      $company = $user->company;
      $company->address          = Input::get('company_address');
      $company->city             = Input::get('company_city');
      $company->postal_code      = Input::get('company_postal_code');
      $company->save();

      // redirect
      Session::flash('message', 'Successfully updated user!');
      return Redirect::to('admin/');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    return view('Users/show', ['user' => Users::findOrFail($id)]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    // get the nerd
    $user = Users::find($id);

    // show the edit form and pass the nerd
    return view('Users/edit', ['user' => $user]);

  }

  /**
   * Update the specified resource in storage.
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

    // validate
    // read more on validation at http://laravel.com/docs/validation
    $rules = array(
        'lastname'    => 'required',
        'firstname'   => 'required',
        'email'       => 'required|email',
        'permissions' => 'max:200',
        'address'     => 'max:100',
        'city'        => 'max:50',
        'postal_code' => 'max:15',
        'birthday'    => 'date'
    );

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->fails()) {
      return Redirect::to('admin/user/edit/' . $id . '')
          ->withErrors($validator);
    } else {

      $user = Users::find($id);
      $user->lastname                 = Input::get('lastname');
      $user->firstname                = Input::get('firstname');
      $user->email                    = Input::get('email');
      $user->permissions              = Input::get('permissions');
      $user->save();

      $information = $user->information;
      $information->birthday         = Input::get('birthday');
      $information->address          = Input::get('address');
      $information->city             = Input::get('city');
      $information->postal_code      = Input::get('postal_code');
      $information->save();

      $company = $user->company;
      $company->address          = Input::get('company_address');
      $company->city             = Input::get('company_city');
      $company->postal_code      = Input::get('company_postal_code');
      $company->save();

      // redirect
      Session::flash('message', 'Successfully updated user!');
      return Redirect::to('admin/user');
    }
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    // delete BDD client
    Schema::drop($company->name);

    // delete from UsersInformations
    $information = $user->information;
    $information->delete();

    // delete from Companies
    $company = $user->company;
    $company->delete();

    // delete from Users
    $user = Users::find($id);
    $user->delete();

    // redirect
    Session::flash('message', 'Successfully deleted the user!');
    return Redirect::to('admin/user');
  }

}

?>
