<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Users;
use App\Companies;
use App\Tables;
use App\Banks;
use App\Subscriptions;
use App\UsersInformations;
use Validator;
use Input;
use Redirect;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;





class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {


            $validator = Validator::make($request->all(), [
                'lastname' => 'required',
                'firstname' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6',
                'password_confirmation' => 'required|same:password',
                'phone' => 'required|max:10|',
                'company' => 'required',
                'job' => 'required'
            ]);

            if ($validator->fails()) {

                return redirect('register')
                    ->withErrors($validator)
                    ->withInput();
            }else{


                $lastname = $request->input('lastname');
                $firstname = $request->input('firstname');
                $email = $request->input('email');
                $password = $request->input('password');
                $password_confirmation = $request->input('password_confirmation');
                $phone = $request->input('phone');
                $company = $request->input('company');
                $job = $request->input('job');


                $companies = new Companies;
                $companies->name = $company;
                $companies->save();

                $user = new Users;
                $user->email = $email;
                $user->lastname = $lastname;
                $user->firstname = $firstname;
                $user->password = bcrypt($password);
                $user->id_companies = $companies->id;
                $user->save();

            }
            //return Redirect::to('abonnement',array('id' => $user->id));
            return redirect()->action(
                'RegisterController@abonnement', ['id' => $user->id]
            );

        }
    }

    public function abonnement($id)
    {

        $user = Users::find($id);
        return view('abonnement', ['user' => $user]);


    }

    public function registerAbonnement(Request $request)
    {



      if($request->isMethod('post'))
      {


          $idUser = $request->input('id_user');


          $user = Users::find($idUser);
            //Update des companies
          $company = Companies::find($user->id_companies);
          $company->address                 = $request->input('Adresse');
          $company->city                = $request->input('Rue');
          $company->postal_code                = $request->input('CP');

          $company->save();



          //Inscription du type d'abonnement
          $sub = new Subscriptions();
          $sub->level = $request->input('abonnement');
          $sub->id_users = $idUser;
          $sub->starting_date = date('Y-m-d');

          $sub->save();

          //Inscription du type d'abonnement

          $info_user = new UsersInformations();
          $info_user->address = $request->input('Adresse');
          $info_user->city = $idUser;
          $info_user->postal_code = date('Y-m-d');
          $info_user->id_users = $idUser;
          $info_user->save();
          //Inscription de la banque

          $bank = new Banks();
          $bank->iban = $request->input('NumCarte');
          $bank->bic = $request->input('Cyptogramme');
          $bank->id_subscriptions = $sub->id;
          $bank->save();

          //Création de la base de donnée
          $nameCompany = $company->name;

          DB::statement("CREATE DATABASE $nameCompany; ");

          //CRÉATION DU DOSSIER
          $path = public_path().'/../environments/'.$nameCompany;


          File::makeDirectory($path, $mode = 0777, true, true);
          //ON COPIE LE .ENV
          copy(public_path()."/../environments/default/.env", $path."/.env");
            //On créer la ligne de connexion en bdd
          $filename = $path."/.env";
          $somecontent = "\nDB_DATABASE_CUSTOMER=".$nameCompany;

          if (is_writable($filename)) {

              if (!$handle = fopen($filename, 'a')) {
                  echo "Impossible d'ouvrir le fichier ($filename)";
                  exit;
              }

              // Ecrivons quelque chose dans notre fichier.
              if (fwrite($handle, $somecontent) === FALSE) {
                  echo "Impossible d'écrire dans le fichier ($filename)";
                  exit;
              }

              //echo "L'écriture de ($somecontent) dans le fichier ($filename) a réussi";

              fclose($handle);

          } else {
              //echo "Le fichier $filename n'est pas accessible en écriture.";
          }


          return redirect('/')->with('status', 'Inscription terminée');
          

      }

    }



}
