<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Users;
use App\Sessions;
use Barryvdh\DomPDF\ServiceProvider;
use Session;
use App;

class UsersApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Users::find($id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        //Utiliser xxx-www-form dans postman
        $user = Users::find($id);

        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');


        $password = $request->input('password');
        $password = $request->input('password');
        $user->email = $request->input('email');
        $user->save();

        $data = array(
            'error' => false,
            'message' => 'Message bien édité.'
        );

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inVoice($id)
    {
        $user = Users::find($id);

        $date = new \DateTime();
        $inVoice = '
    <div style="max-width: 800px;margin: auto;padding: 30px;border: 1px solid #eee;box-shadow: 1px 1px 10px rgba(0,0,0,0.15);font-size: 16px;line-height: 24px;color: #555;">
        <table>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td width="250" style="text-align: left">
                                <img src="'.public_path().'/img/logo-intersession.png" style="width:100%; max-width:150px;height:auto;">
                            </td>
                            <td width="250" style="text-align: right">
                                Facture #: 123<br>
                                Du: January 1, 2015<br>
                                Au: February 1, 2015
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td width="250" style="text-align: left">
                                IPSSI<br>
                                Charpenne, Lyon <br>
                                Lyon, 42000
                            </td>
                            <td width="250" style="text-align: right">
                                John Doe
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="250" style="text-align: left;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                    Mode de paiement
                </td>
                <td width="250" style="text-align: right;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                    A payer
                </td>
            </tr>
            <tr class="details">
                <td width="250" style="text-align: left">
                    CB
                </td>
                <td width="250" style="text-align: right">
                    250
                </td>
            </tr>
            <tr>
                <td width="250" style="text-align: left;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                    Produit
                </td>
                <td width="250" style="text-align: right;background: #eee;border-bottom: 1px solid #ddd;font-weight: bold;">
                    Prix
                </td>
            </tr>
            <tr class="total">
                <td>
    
                </td>
                <td width="250" style="text-align: right">
                    Total: 250€
                </td>
            </tr>
        </table>
    </div>';


    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML($inVoice);
    return $pdf->stream();

    }

    public static function timeByPeriod($id, $dateOne, $dateTwo)
    {
      // we transform the timestamp to datetime and after a string
      $startingDate = new \DateTime();
      $startingDate->setTimestamp($dateOne);
      $startingDate = $startingDate->format('Y-m-d');
      $endingDate = new \DateTime();
      $endingDate->setTimestamp($dateTwo);
      $endingDate = $endingDate->format('Y-m-d');

      // we recover all sessions for the user
      $sessions = Sessions::all()->where('id_users', $id);
      $i = 0;

      foreach ($sessions as $key => $value) {
        // we transform the session datetime to date string
        $startingDateSession = new \DateTime($value->starting_date);
        $startingDateSession = $startingDateSession->format('Y-m-d');
        $endingDateSession = new \DateTime($value->ending_date);
        $endingDateSession = $endingDateSession->format('Y-m-d');

        if(strtotime($startingDateSession) >= strtotime($startingDate) && strtotime($endingDateSession) <= strtotime($endingDate) && $value->token <> Session::get('token')){
          // we transform the session datetime to datetime string
          $startingDateSession = new \DateTime($value->starting_date);
          $startingDateSession = $startingDateSession->format('Y-m-d H:i:s');
          $endingDateSession = new \DateTime($value->ending_date);
          $endingDateSession = $endingDateSession->format('Y-m-d H:i:s');

          $i += strtotime($endingDateSession) - strtotime($startingDateSession);
        }
      }

      return $i;
    }
}
