<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Schema;
use Input;
use DB;
use Driver;
use Illuminate\Http\Request;
use App\Tables;
use App\Columns;

class ColumnsApiController extends Controller
{
    /**
     * Update the specified resource in storage.
     * NOT USE, NOT FINISH
     * @return Response
     */
    public function update(Request $request)
    {
        $tableContent = $request->input('data');
        $table = Tables::where('id', $tableContent['id_tables'])->first();

        if(!$table){
            return ['error' => false, 'message' => "Il n'a pas de table correspondant à votre requête !"];
        }

        $tableName = Tables::slugify($table->name);

        if (Schema::connection('mysql_customer')->hasTable($tableName)) {

            foreach ($tableContent['content'] as $key => $value) {

                DB::connection('mysql_customer')->table($tableName)->where('id', $tableContent['content']['id'])->update([
                    'updated_at' => new \DateTime(),
                    Tables::slugify($key) => $value,
                ]);
            }

            return ['header' => Tables::getArrayTableHeader($tableContent['id_tables']), 'content' => Tables::getArrayTableContent($tableName), 'message' => 'Ligne modifiée !'];
        } else {
            return ['error' => false, 'message' => "La ligne n'existe pas !"];
        }
    }
}
