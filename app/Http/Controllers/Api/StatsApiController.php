<?php

namespace App\Http\Controllers\Api;

use App\Users;
use App\Providers\StatsServiceProvider as Stats;
use App\Http\Controllers\Controller;

class StatsApiController extends Controller
{
    public function show($periode)
    {

        $devices = Stats::getDeviceCategory($periode);
        $top5PageVisit = Stats::getTop5PageVisit($periode);
        $totalVisitsViews = Stats::getStatsVisitsViewsTotal($periode);
        $bounceRate = Stats::getVisitBounceRate($periode);
        $avgTimeOnSite = Stats::getAvgTimeOnSite($periode);


        $return['nbUsers'] = Users::count();
        $return['visitsDevices'] = $devices;
        $return['topVisits'] = $top5PageVisit;
        $return['totalVisitsViews'] = $totalVisitsViews;
        $return['bounceRate'] = $bounceRate;
        $return['avgTimeOnSite'] = $avgTimeOnSite;

        return response()->json($return);
    }
}
