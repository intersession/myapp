<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Schema;
use Input;
use DB;
use Driver;
use Illuminate\Http\Request;
use App\Tables;
use App\Columns;
use App\Projects;
use App\Subscriptions;
use App\Users;

class TablesApiController extends Controller
{

    public function getAll(Request $request)
    {
        return Tables::getArrayTableHeader();
    }

    public function get(Request $request, $id)
    {
        $tableName = Tables::slugify(Tables::where('id', $id)->first()->name);

        if (Schema::connection('mysql_customer')->hasTable($tableName)) {
            return ['header' => Tables::getArrayTableHeader($id), 'content' => Tables::getArrayTableContent($tableName)];
        } else {
            return ['error' => false, 'message' => "La table n'existe pas !"];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $tableContent = $request->input('data');

        // subscription level
        if(!$this->allowAccesCreateTable($tableContent['id_projects'])){
            return ['error' => false, 'message' => "Vous êtes sur un abonnement gratuit, le nombre de création de table a été atteint !"];
        }

        // main
        $tableName = Tables::slugify($tableContent['name']);

        if (!Schema::connection('mysql_customer')->hasTable($tableName)) {

            // create schema table
            Schema::connection('mysql_customer')->create($tableName, function($table) use ($tableContent)
            {
                $table->bigIncrements('id');
                $table->timestamps();

                $table = $this->arrayProcessing($tableContent, $table);
            });

            // create row table
            $id = DB::connection('mysql_api')->table('tables')->insertGetId([
                'name' => $tableContent['name'],
                'description' => $tableContent['description'],
                'id_projects' => $tableContent['id_projects'],
            ]);

            // create rows column
            foreach ($tableContent['columns'] as $key => $value) {
                DB::connection('mysql_api')
                ->table('columns')
                ->insert([
                    'type' => $value['type'],
                    'required' => $value['required'],
                    'name' => $value['name'],
                    'id_tables' => $id,
                ]);
            }

            return ['data' => Tables::getArrayTableHeader(), 'message' => "Table ajoutée !"];
        } else {
            return ['error' => false, 'message' => "La table existe déjà !"];
        }
    }

    /**
    * @param Integer $idProject
    * @return Boolean
    * Allow acces create table
    */
    private function allowAccesCreateTable($idProject)
    {
        $project = Projects::where('id', $idProject)->first();
        $nbTablesProject = Tables::where('id_projects', $idProject)->count();

        if($project){
            $user = Users::where('id', $project->id_users)->first();

            if($user){

                foreach (Subscriptions::all()->where('id_users', $user->id) as $key => $value) {

                    if(Subscriptions::hasCurrentSubscription($value)){

                        if($value->level == 0 && $nbTablesProject >= 5){
                            return false;
                        }
                    }
                }
            } else {
                // on a pas d'utilisateur lié
                return false;
            }
        } else {
            // on a pas de projet
            return false;
        }

        return true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $table = Tables::where('id', $id)->first();

        if(!$table){
            return ['error' => false, 'message' => "Il n'a pas de table correspondant à votre requête !"];
        }

        $tableContent = $request->input('data');
        $tableName = Tables::slugify($table->name);

        if (Schema::connection('mysql_customer')->hasTable($tableName)) {
            // update schema table
            Schema::connection('mysql_customer')->rename($tableName, Tables::slugify($tableContent['name']));

            // update row table
            DB::connection('mysql_api')->table('tables')->where('id', $id)->update([
                'name' => $tableContent['name'],
                'description' => $tableContent['description'],
            ]);

            return ['data' => Tables::getArrayTableHeader(), 'message' => "Table modifiée !"];
        } else {
            return ['error' => false, 'message' => "La table n'existe pas !"];
        }
    }

    /**
    * @param Array $tableContent
    * @param Table Connection $table
    * @return Table Connection $table
    * Processing array value column
    */
    private function arrayProcessing($tableContent, $table)
    {
        foreach ($tableContent['columns'] as $content) {

            switch ($content['type']) {
                case "mail":
                    $table->string(Tables::slugify($content['name']));
                    break;

                case "txt":
                    $table->string(Tables::slugify($content['name']));
                    break;

                case "float":
                    $table->float(Tables::slugify($content['name']));
                    break;

                case "longtxt":
                    $table->longText(Tables::slugify($content['name']));
                    break;
            }
        }

        return $table;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $table = Tables::where('id', $id)->first();

        if(!$table){
            return ['error' => false, 'message' => "Il n'a pas de table correspondant à votre requête !"];
        }

        $tableName = Tables::slugify($table->name);

        if (Schema::connection('mysql_customer')->hasTable($tableName)) {
            // destroy schema table
            Schema::connection('mysql_customer')->drop($tableName);

            DB::connection('mysql_api')->table('columns')->where('id_tables', $id)->delete();
            DB::connection('mysql_api')->table('tables')->where('id', $id)->delete();

            return [Tables::getArrayTableHeader(), 'message' => "Table supprimée !"];
        } else {
            return ['data' => false, 'message' => "Table inexistante !"];
        }
    }
}
