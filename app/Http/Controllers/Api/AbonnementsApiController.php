<?php

namespace App\Http\Controllers\Api;

use App\Subscriptions;
use App\Providers\StatsServiceProvider as Stats;
use App\Http\Controllers\Controller;


class AbonnementsApiController extends Controller
{
    public function show($id)
    {
        //On retourne les infos de l'abonnement du client
        return Subscriptions::find($id);

    }
}
