<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Schema;
use Input;
use DB;
use Driver;
use Illuminate\Http\Request;
use App\Tables;
use App\Columns;

class RowsApiController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $tableContent = $request->input('data');
        $table = Tables::where('id', $tableContent['id_tables'])->first();

        if(!$table){
            return ['error' => false, 'message' => "Il n'a pas de table correspondant à votre requête !"];
        }

        $tableName = Tables::slugify($table->name);

        if (Schema::connection('mysql_customer')->hasTable($tableName)) {

            $insertRow = [
                'created_at' => new \DateTime(),
                'updated_at' => new \DateTime()
            ];

            // create row table
            foreach ($tableContent['content'] as $key => $value) {
               $insertRow[Tables::slugify($key)] = $value;
            }

            DB::connection('mysql_customer')->table($tableName)->insert($insertRow);

            return ['header' => Tables::getArrayTableHeader($tableContent['id_tables']), 'content' => Tables::getArrayTableContent($tableName), 'message' => 'Ligne créée !'];
        } else {
            return ['error' => false, 'message' => "La table n'existe pas !"];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $tableContent = $request->input('data');
        $table = Tables::where('id', $tableContent['id_tables'])->first();

        if(!$table){
            return ['error' => false, 'message' => "Il n'a pas de table correspondant à votre requête !"];
        }

        $tableName = Tables::slugify($table->name);

        if (Schema::connection('mysql_customer')->hasTable($tableName)) {

            // create row table
            foreach ($tableContent['content'] as $key => $value) {

                DB::connection('mysql_customer')->table($tableName)->where('id', $tableContent['content']['id'])->update([
                    'updated_at' => new \DateTime(),
                    Tables::slugify($key) => $value,
                ]);
            }

            return ['header' => Tables::getArrayTableHeader($tableContent['id_tables']), 'content' => Tables::getArrayTableContent($tableName), 'message' => 'Ligne modifiée !'];
        } else {
            return ['error' => false, 'message' => "La ligne n'existe pas !"];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $idtable, $id)
    {
        // $tableContent = $request->input('data');
        // dd($tableContent);
        $table = Tables::where('id', $idtable)->first();

        if(!$table){
            return ['error' => false, 'message' => "Il n'a pas de table correspondant à votre requête !"];
        }

        $tableName = Tables::slugify($table->name);

        if (Schema::connection('mysql_customer')->hasTable($tableName)) {
            DB::connection('mysql_customer')->table($tableName)->where('id', $id)->delete();

            return ['header' => Tables::getArrayTableHeader($idtable), 'content' => Tables::getArrayTableContent($tableName), 'message' => 'Ligne supprimée !'];
        } else {
            return ['error' => false, 'message' => "La ligne n'existe pas !"];
        }
    }
}
