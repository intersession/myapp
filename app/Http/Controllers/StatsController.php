<?php 

namespace App\Http\Controllers;

use App\Users;
use App\Providers\StatsServiceProvider as Stats;
use App\Http\Controllers\Controller;



class StatsController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index($periode = 'D')
  {
    $devices = Stats::getDeviceCategory($periode);
    $top5PageVisit = Stats::getTop5PageVisit($periode);
    $totalVisitsViews = Stats::getStatsVisitsViewsTotal($periode);
    $bounceRate = Stats::getVisitBounceRate($periode);
    $avgTimeOnSite = Stats::getAvgTimeOnSite($periode);


    //$return['nbUsers'] = Users::count();
    $return['visitsDevices'] = $devices;
    $return['topVisits'] = $top5PageVisit;
    $return['totalVisitsViews'] = $totalVisitsViews;
    $return['bounceRate'] = $bounceRate;
    $return['avgTimeOnSite'] = $avgTimeOnSite;

    return view('stats',$return);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
    
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>