<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\Roles;
use App\RolesUsers;
use App\Companies;
use App\Sessions;
use App\Projects;
use Session;
use Auth;
use Hash;
use Password;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    protected $redirectPath = '/admin';
    protected $loginPath = 'auth/login';
    protected $redirectAfterLogout = '/';
    protected $guard = '/admin';

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {


        $this->middleware('guest', ['except' => 'getLogout']);


    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function authentication(Request $request)
    {
      $email = $request->input('email');
      $password = $request->input('password');

      // we recover the user entity
      $user = Users::where('email', $email)->first();

      // if the user exist
      if($user && Hash::check($password, $user->password)){
        // ROLES
        $arrayRoles = array();
        $rolesUser = RolesUsers::all()->where('id_users', $user->id);
        foreach ($rolesUser as $key => $value) {
          $arrayRoles[] = Roles::find($value->id_roles)->name;
        }

        // USER
        $user->id_companies = Companies::find($user->id_companies);
        // PROJECT
        $project = Projects::where('id_users', $user->id)->first();

        // SESSION
        $token = bin2hex(openssl_random_pseudo_bytes(16));
        $dateTime = new \DateTime();
        $dateTime->setTimestamp(time() + 1800);

        DB::table('sessions')->insert([
          'id_users' => $user->id,
          'starting_date' => new \DateTime(),
          'ending_date' => $dateTime->format('Y-m-d H:i:s'),
          'token' => $token,
          'date' => new \DateTime()
        ]);

        Session::put('token', $token);
        Session::put('idUser', $user->id);

        if($request->input('type') == 'local' && Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect()->intended('admin_dashboard');
        }

        return ['login' => 'true', 'user' => $user, 'roles' => $arrayRoles, 'session_token' => $token, 'exp' => time() * 1000 + 1800000, 'project' => $project];
      } else {
        return ['login' => 'false', 'message' => "L'email ou le mot de passe ne correspondent pas !"];
      }
    }

    /*
    * Logout a user
    */
    protected function getLogout(Request $request)
    {
      DB::table('sessions')->where('token', $request->input('token'))->update([
        'ending_date' => new \DateTime(),
      ]);

      if($request->input('token') == Session::get('token')){
        Session::forget('token');
        Session::forget('idUser');
      }

      return response()->json(['logout' => 'true', 'message' => 'Vous êtes bien déconnecté !']);
    }

    /*
    * send a mail
    */
    public function forgetPassword(Request $request)
    {
        $email = $request->input('email');
        $password = substr(md5($email),0,8);
        $user = Users::where('email', $email)->first();

        if($user){
            Mail::send('email', ['password' => $password, 'name' => $user->firstname . " " . $user->lastname], function($message) use($email) {
                $message->to($email)->subject('BMS - Mot de passe oublié');
            });

            Users::where('email', $email)->update([
                'password' => bcrypt($password),
            ]);

            return response()->json(['forgetpassword' => 'true', 'message' => 'Un e-mail contenant votre nouveau mot de passe a été envoyé !']);
        } else {
            return response()->json(['forgetpassword' => 'false', 'message' => 'E-mail non reconnu !']);
        }
    }
}
