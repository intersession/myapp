<?php

namespace App\Http\Controllers;

use App\Repositories\PagesRepository;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{

    public $pagesRepository;

    public $nbrPerPage = 12;

    public function __construct(PagesRepository $PagesRepository)
    {
        $this->pagesRepository = $PagesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = $this->pagesRepository->getPaginate($this->nbrPerPage);
        $links = $pages->setPath('')->render();
        // dd($pages);
        return view('pages-index', compact('pages', 'links'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->pagesRepository->getById($id);

        return view('pages-edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->pagesRepository->update($id, $request->all());

        return redirect('pages')->withOk("La page " . $request->input('name') . " a été modifiée.");
    }

    /**
     * Display the presentation page.
     *
     * @return \Illuminate\Http\Response
     */
    public function displayPresentation()
    {
        $presentationGenerale = $this->pagesRepository->getById(1);
        $service1 = $this->pagesRepository->getById(2);
        $service2 = $this->pagesRepository->getById(3);
        $service3 = $this->pagesRepository->getById(4);
        $accroche = $this->pagesRepository->getById(5);

        return view('presentation', compact('presentationGenerale','service1','service2','service3','accroche'));
    }

    /**
     * Display the presentation page.
     *
     * @return \Illuminate\Http\Response
     */
    public function displayMentions()
    {
        $mentions = $this->pagesRepository->getById(6);

        return view('mentions-legales', compact('mentions'));
    }

    /**
     * Display the presentation page.
     *
     * @return \Illuminate\Http\Response
     */
    public function displayCgv()
    {
        $cgv = $this->pagesRepository->getById(7);

        return view('cgv', compact('cgv'));
    }
}
