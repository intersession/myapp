<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Routing\Router;
use Illuminate\Contracts\Foundation\Application;

class Kernel extends HttpKernel
{

    public function __construct(Application $app, Router $router)
    {

        if (isset($_SESSION['appName'])) {
            $app->useEnvironmentPath(base_path() . '/environments/env'. $_SESSION['appName'] .'/'); // here you can customize the path.
        }
        else {
            $app->useEnvironmentPath(base_path() . '/'); // here you can customize the path.
        }
        parent::__construct($app, $router);
    }
    
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
     //   \App\Http\Middleware\VerifyCsrfToken::class, //EN COMMENTAIRE SINON LES API POST MARCHENT PAS

    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
    ];
}
