<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model {

	protected $table = 'roles';
	public $timestamps = false;

	const SUPERADMIN = 'superadmin';
	const ADMIN = 'admin';
	const COLLABORATOR = 'collaborator';

	public function hasRolesUsers()
	{
		return $this->hasMany('App\RolesUsers');
	}

}