<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model {

	protected $table = 'subscriptions';
	public $timestamps = false;

	public function hasBank()
	{
		return $this->hasOne('App\Banks');
	}

	public static function hasCurrentSubscription($subscription)
	{
		$startingDate = new \DateTime($subscription->starting_date);
		$endingDate = new \DateTime($subscription->ending_date);
		$currentDate = new \DateTime();

		if($currentDate->getTimestamp() >= $startingDate->getTimestamp() && $currentDate->getTimestamp() < $endingDate->getTimestamp()){
			return true;
		}

		return false;
	}

	public static function getNameLevel($subscription)
	{
		switch ($subscription->level) {
			case 0:
				return 'Gratuit';
				break;

			case 1:
				return 'Premium';
				break;
		}

		return 'N/A';
	}
}
