<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use DB;

class Users extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{

	use Authenticatable, Authorizable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['lastname', 'firstname', 'email', 'permissions','password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public $timestamps = false;
	
	/*public function show_user()
	{
		return Users::join('companies', 'users.id_companies', '=', 'companies.id')
            ->select('users.*', 'companies.name')
            ->get();
	}*/

	public function company() {
		return $this->hasOne('App\Companies', 'id', 'id_companies');
	}

	public function information() {
		return $this->hasOne('App\UsersInformations', 'id_users', 'id');
	}

	public function abonnement() {
		return $this->hasOne('App\Subscriptions', 'id_users', 'id');
	}
}