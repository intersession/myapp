<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersInformations extends Model {

	protected $table = 'users_informations';
	public $timestamps = false;

}