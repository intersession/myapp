<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entities extends Model {

	protected $table = 'entities';
	public $timestamps = false;

	public function __construct()
	{
		parent::setConnection('mysql_api');
	}

}