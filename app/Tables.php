<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tables extends Model {

	protected $table = 'tables';
	public $timestamps = false;

	public function __construct()
	{
		parent::setConnection('mysql_api');
	}

	public function hasColumns()
	{
		return $this->hasMany('App\Columns');
	}

	/**
	* @param $string
	* @return mixed|string
	* Generate slug from given string
	*
	*/
	public static function slugify($string)
	{
		$string = preg_replace('~[^\pL\d]+~u', '_', $string);
		$string = iconv('utf-8', 'us-ascii//TRANSLIT', $string);
		$string = preg_replace('~[^-\w]+~', '', $string);
		$string = trim($string, '_');
		$string = preg_replace('~-+~', '_', $string);
		$string = strtolower($string);

		return $string;
	}

    /**
	* @param Intger $id
    * @return Array $table
    * Get table and columns array
    */
    public static function getArrayTableHeader($id = 0)
    {
        $tables = [];
        $table = [];
        $array = [];

        if($id <= 0) {
            $array = Tables::all()->toArray();

            foreach ($array as $keyTable => $valueTable) {

                foreach ($valueTable as $key => $value) {

                    if($key == 'id'){
                        $table['columns'] = Columns::all()->where('id_tables', $value)->toArray();
                    }

                    $table[$key] = $value;
                }

                $tables[$keyTable] = $table;
            }

            return $tables;
        } else {
            $array = Tables::where('id', $id)->first()->toArray();

            foreach ($array as $keyTable => $valueTable) {

                if($keyTable == 'id'){
                    $table['columns'] = Columns::all()->where('id_tables', $valueTable)->toArray();
                }

                $table[$keyTable] = $valueTable;
            }

            return $table;
        }
    }

    /**
	* @param String $tableName
    * @return Array $table
    * Get table and columns array
    */
    public static function getArrayTableContent($tableName)
    {
		$table = DB::connection('mysql_customer')->table($tableName)->get();
		$tableReturn = [];

		foreach ($table as $key => $value) {

			foreach ($value as $keyArray => $valueArray) {

				if($keyArray == 'id' || $keyArray == 'created_at' || $keyArray == 'updated_at'){
					$tableReturn[$key][$keyArray] = $valueArray;
				} else {
					$tableReturn[$key]['columns'][$keyArray] = $valueArray;
				}
			}
		}

		return $tableReturn;
    }
}
