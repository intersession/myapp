## BMS && Laravel PHP Framework

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Install BMS

Se placer sur la branche sprint : git checkout [sprint-xxx]
Récupérer les dernières infos : git pull origin [sprint-xxx]
Créer une branche locale : git checkout -b [branche/locale]
Mettre à jour Laravel : composer install
Créer la base de données : php artisan migrate
Générer les fixtures : php artisan db:seed

## Push des changements
Enregistrement des changements : git add -A && commit -m "[message in english]"
On se place sur la branche de dev : git checkout [sprint-xxx]
On récupère les infos : git merge [branche/locale]
On pousse les infos : git push origin [sprint-xxx]

## Generate PDF
sudo apt-get install php5-gd && sudo service apache2 restart